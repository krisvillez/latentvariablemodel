LVM toolbox v1.0 - Latent Variable Model toolbox
by Stefania Russo, Guangyu Li, Kris Villez

Website:	http://homepages.eawag.ch/~villezkr/spike/
Contact: 	kris.villez@eawag.ch

This toolbox is a collection of Matlab/Octave functions and scripts originally written to benchmark the ignorance score as a criterion for model selection in principal component analysis. They are released to the public under the Apache License 2.0 in order to encourage the sharing of code and prevent duplication of effort. If you find these functions useful, drop me a line. I also appreciate bug reports and suggestions for improvements. For more detail regarding copyrights see the LICENSE file located in the same folder where this README file is located.

These files are provided as is, with no guarantees and are intended for non-commercial use. These routines have been developed and tested successfully with Matlab (R2017b) on a Windows 10 system. Tests in Octave are incomplete at the time of writing. To use the Matlab scripts and functions in a Windows system, using Matlab or Octave, follow the instructions below.

A. COMPUTATIONS
===============

1. Simulated data sets
----------------------

To repeat the computation of our results, execute the following scripts:
	./applications/Russo_et_al_2019_simulation/s_simulation_B1.m
	./applications/Russo_et_al_2019_simulation/s_simulation_B2.m
	./applications/Russo_et_al_2019_simulation/s_simulation_C.m

2. Experimental data sets
-------------------------

To repeat the computation of our results, execute the following scripts:
	./applications/Russo_et_al_2019_experimental/script_crossvalidate_CoII_CrIII_NiII.m
	./applications/Russo_et_al_2019_experimental/script_crossvalidate_NO2_NO3.m


B. VISUALIZATION
================

1. Simulated data sets
----------------------

To visualize the results of the benchmarking with one of simulated data sets, execute the following scripts:
	./applications/Russo_et_al_2019_simulation/script_figures.m
	./applications/Russo_et_al_2019_simulation/script_figure_combination.m

In the first script, modify line 45 ("CASE = 3;") by using 1 for class B1, 2 for class B2, and 3 for classes C1, C2, C3, and C4

2. Experimental data sets
-------------------------

To visualize the results of the benchmarking with the experimental data sets, execute the following script:

	./applications/Russo_et_al_2019_experimental/script_figures.m


