
% -------------------------------------------------------------------------
% PCA dimensionality selection - script_setC1toC4.m
%
% Script which tests dimensionality selection methods on simulate data set
% C1 to C4 [1]. 
%
% [1] https://arxiv.org/abs/1902.03293
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 21.05.2019
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Stefania Russo, Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
clear all
close all
more off

addpath('../../tools')

%% BENCHMARKING STUDY SETUP
% Number of repetitions:
nRep    =   100 ;
% Data sets, given as:
SetName = 'C' ;
Types = 4 ; % 
%NoiseLevels = [1 2:2:10 ]; % as in IECR
NoiseLevels = [1:11 ]; % as in Watermatex2019

% Model/method combinations, given as:
%               model type	/	centering / scaling /	CV-pattern /	criterion
GetMethods

% Number of simulated data samples:
nSample     =   1024	;
nBlock      =   16      ;   % cross-validation blocks
J           =   100     ;   % An upper bound for the number of OVs across all data sets

path_local  =   erase( mfilename( 'fullpath' ), mfilename () );
path_out	=	[ path_local 'result_set' SetName '\' ]	;

CrossValidate_Simulation
