
% -------------------------------------------------------------------------
% PCA dimensionality selection - script_figures.m
%
% Script which produces figures for the manuscript [1]. 
%
% [1] https://arxiv.org/abs/1902.03293
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 21.05.2019
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Stefania Russo, Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

more off
clc
clear all
close all

fig4manuscript = false ;
addpath('../../tools')

try 
    addpath('C:\G\code\spike_b')
catch
end

CASE = 3;
switch CASE
    case 1
        SetName     =	'setB1' ;
        Types       =	1:18	; %
        NoiseLevels =	1       ; % as in current manuscript
        nRep        =	100     ;
    case 2
        SetName     =	'setB2'	;
        Types       =	1:18	; %
        NoiseLevels	=	1       ; % as in current manuscript
        nRep        =   100     ;
    case 3
        SetName     =	'setC'	;
        Types       =	1:2     ; %
        NoiseLevels	=	1:6     ; % as in current manuscript
        nRep        =   100      ;
end

Subset	=	'ALL'       ;
%Subset	=	'firsttwo'	;

switch Subset
    case 'ALL'
        ViewMethod = [ 1:11 ];
    otherwise
        ViewMethod = [ 1:2 ];
end
addpath('tools')
markersub       =	{'<','>','o','v'}       ;

warning('off','MATLAB:table:ModifiedAndSavedVarnames')

path_figures	=       '.\result_figures\'         ;
path_in         =	[	'.\result_' SetName '_R2017b\'	]	;
path_out        =	[	'.\result_' SetName '_R2017b\' ]	;

%path_in         =	[	'.\result_' SetName '\'	]	;
%path_out        =	[	'.\result_' SetName '\' ]	;

colours         =	[	241,163,64	;   ...
                        153,142,195	]/255 ;
%% READ DESIGN
disp('Loading design ...')
filepath        =	[	path_in	'design.csv'	]	;

fid             =       fopen(filepath)             ;
for r=1:7
    tline       =       fgetl(fid)                  ;
    loc         =       strfind(tline, ';')         ;
    switch r
        case 1
            nBlock	=   str2num(tline((loc(1)+1):(loc(2)-1)))   ;
        case 2
            nOVmax	=   str2num(tline((loc(1)+1):(loc(2)-1)))   ;
        case 3
            %nRep    =   str2num(tline((loc(1)+1):(loc(2)-1)));
        case 4
            nSample	=   str2num(tline((loc(1)+1):(loc(2)-1)))   ;
        case 5
            SetName	=   tline((loc(1)+1):(loc(2)-1))            ;
        case 6
            Types	=	strsplit(tline,';')                     ;
            Types	=	str2num(cell2mat(Types(2:end-1)'))      ;
        case 7
            NoiseLevels	=	strsplit(tline,';')                         ;
            NoiseLevels =	str2num(cell2mat(NoiseLevels(2:end-1)'))	;
    end
            
end


fclose(fid)	;

tab         =	readtable(filepath,'Delimiter',';','HeaderLines',7)	;
tab         =	tab(:,1:end-1)      ;
Methods     =	table2cell(tab)     ;

% META-DATA
nType       =   length(Types)       ;
nNoise      =   length(NoiseLevels)	;
nMethod     =   size(Methods,1)     ;

Impute                      =       upper(char(Methods{:,5}))	;
Impute(Impute(:,1)=='C',1)	=       'c'                         ;
MethodNames0                =	[	upper(char(Methods{:,4})) repmat(' ',[nMethod 1]) Impute	]   ;


MethodNames	=	upper(Methods(:,5)) ;
switch Subset
    case 'ALL'
        MethodNames{9}	=	[	MethodNames{9}	'-EKF'	]   ;
        MethodNames{10}	=	[	MethodNames{10}	'-RKF'	]   ;
    otherwise
end
MethodNames =char(MethodNames);
switch Subset
    case 'ALL'
        MethodNames(5:8,1)	=   'c'	;
    otherwise
end
MethodNamesCell         =       cellstr(MethodNames)        ;

disp('Loading design done ')

%% PROCESS DATA
disp('Processing data ... ')

% PRE-ALLOCATION
CountCorrect    =	zeros(nType,nNoise,nMethod) ;
TimeUsed        =	zeros(nType,nNoise,nMethod) ;
CountPC         =   zeros(nType,nNoise,nMethod,nOVmax+1)	;
Ktrue           =	nan(nType,1)                            ;
Kmax            =	nan(nType,1)                            ;

CRIT = [];
% LOOPS: ACTUAL BENCHMARKING
for iType =1:nType
    for iNoise= 1:nNoise
        
        for iMethod=1:nMethod
            model       =	Methods{iMethod,1}	;
            centering	=	Methods{iMethod,2}	;
            scaling     =	Methods{iMethod,3}	;
            pattern     =	Methods{iMethod,4}	;
            criterion	=	Methods{iMethod,5}	;
            
            MethodName	=	[ 'Model' model '-' centering '-' scaling '_CV' pattern '-' criterion ]       ;
            filepath    =   [ path_out 'Set' SetName '.' num2str(iType,'%02d') '.' num2str(iNoise) '_' MethodName  '.csv' ]	;
                
            tab         =	readtable(filepath) ;
            tab         =	tab(:,1:end-1)      ;
            tab         =	table2array(tab)    ;
            
            Ktrue(iType)=	find(tab(1,3:end))-1	;
            timeused    =	tab(3:end,2)            ;
            criterion   =	tab(2+(1:nRep),3:end)   ;
            
            
            [~, index]	=	sort(criterion, 2)  ;
            Khat        =	index(:,1)-1        ;
            Kmax(iType) =	size(criterion,2)-1 ;
            N           =   histcounts(Khat,'BinLimits',[0,nOVmax],'BinMethod','integers')	;
            
            TimeUsed(iType,iNoise,iMethod)      =	mean(timeused)      ;
            CountPC(iType,iNoise,iMethod,:)     =	N                   ;
            CountCorrect(iType,iNoise,iMethod)	=	N(Ktrue(iType)+1)	;
            
            if iType==4
                if iNoise==nNoise
                    CRIT	=	[	CRIT	;	criterion(11,:)	]   ;
                end
            end
        end
    end
end
if CASE==3 && fig4manuscript
    select = [ 1 2:2:10]; % noise levels 5-10-20-30-40-50% 
    TimeUsed = TimeUsed(:,select,:);
    CountPC = CountPC(:,select,:,:);
    CountCorrect = CountCorrect(:,select,:);
    NoiseLevels = NoiseLevels(select);
    nNoise      =   length(NoiseLevels)	;
end
    
disp('Processing data done ')

% FIGURE 42
figure(42),
if exist('FigPrep','file'), FigPrep; end
hold on
%set(gca,'FontName','DejaVu Sans Mono')

colour = { ...
    [0,0,0]/255,...
    [0,123,255]/255,...
    [0,123,255]/255,...
    [202,0,32]/255};
K_ = (1:size(CRIT,2))-1;
for iMethod=1:nMethod
    
    crit            =	CRIT(iMethod,:)     ;
    iMethodclass	=	ceil(iMethod/4 )    ;
    iMethodsub      =	mod(iMethod-1,4)+1  ;
    
    subplot(3,1,iMethodclass), hold on,
    if exist('AxisPrep','file'), AxisPrep; end
    if mod(iMethod,2)==0
        lines	=	':'	;
    else
        lines	=	'-'	;
    end
    plot( -11-K_, crit, lines, 'LineWidth', 2, 'Marker', markersub{iMethodsub}, 'MarkerSize', 11, 'Color', colour{iMethodsub} )
    
    iMethodNames	=	(iMethodclass-1)*4+1:min([(iMethodclass)*4,nMethod])	;
    legend(MethodNames(iMethodNames,:),'AutoUpdate','Off','Location','North')
    grid on
end

for iMethod=1:nMethod
    
    crit            =	CRIT(iMethod,:)         ;
    [~,index]       =	min( crit )             ;
                
    iMethodclass	=	ceil( iMethod/4 )       ;
    iMethodsub      =	mod( iMethod-1,4 ) + 1  ;
    
    subplot(3,1,iMethodclass), hold on,
    if exist('AxisPrep','file'), AxisPrep; end
    if mod(iMethod,2)==0
        lines	=	':'	;
    else
        lines	=	'-'	;
    end
    plot( K_, crit, lines, 'LineWidth', 3, 'Color', colour{iMethodsub} )
    plot( K_(index), crit(index), 'k', 'Marker', markersub{iMethodsub}, 'MarkerSize', 17, 'LineWidth', 2, 'Color', colour{iMethodsub} )
    
    iMethodNames	=	(iMethodclass-1)*4+1:min([(iMethodclass)*4,nMethod])	;
    legend(MethodNames(iMethodNames,:),'AutoUpdate','Off','Location','North')
    set( gca, 'Xlim', [0 K_(end)] )
    
    if CASE==3
        set( gca, 'Xtick', [0:3:K_(end)] )
    else
        set( gca, 'Xtick', [0:1:K_(end)] )
    end
    
    if iMethodclass==3
        set( gca, 'Ylim', [0 +1.7] )
        ylbl	=	{'IGN','VRE'}	;
        xlabel( '# principal components [K]' )
    else
        set( gca, 'Xticklabel', [ ] )
        set( gca, 'Ylim', [0 1.7] )
            
        ylbl	=	{'RMSR'}        ;
    end
    ylabel(ylbl)
    
    grid on
    
    drawnow()
end

figurepath	=	[	path_figures 'fig_'  SetName  '_example_' Subset	]   ;
saveas (gcf,figurepath,'fig')
saveas (gcf,figurepath,'epsc')
saveas (gcf,figurepath,'tiff')

disp( 'Figure exemplary results done ' )

%% SELECT DATA FOR PLOTS
switch Subset
    case 'ALL'
        iMethods =	1:11    ;
    otherwise
        iMethods =	1:2    ;
end
CountPC         =	CountPC(:,:,iMethods,:)     ;
TimeUsed        =	TimeUsed(:,:,iMethods)      ;
CountCorrect	=	CountCorrect(:,:,iMethods)  ;
nMethod         =	length(iMethods)            ;

%% FIGURE 0
disp('Figure non-noisy variance ... ')
nType	=	length(Types)       ;
nNoise	=	length(NoiseLevels)	;

for iType =1:nType
        for iNoise=1:nNoise
            
            disp( [ '	Data set class: ' num2str(SetName) ....
                    ' type: ' num2str(iType) ' of ' num2str(nType) ...
                    ', Noise level: ' num2str(iNoise) ' of ' num2str(nNoise) ] )
            
            % Data generation:
            setname     =   SetName             ;
            type        =	Types(iType)        ;   % set type
            noiselevel	=	NoiseLevels(iNoise)	;   % set type
            
            [Sigma2, J, Ktrue1]	=	fGenerateCovariance( setname, type, noiselevel )	; % currently complete: A,B1,B2,C
            [U,S,V]             =   svd( Sigma2 )                                       ;
            lambda              =	diag(S).^(1/2)                                      ;
            
            F1(iType,iNoise)	=	sum(lambda(1:Ktrue1)-lambda(end))./sum(lambda)      ;
            
            if Ktrue1 ==0
                F2(iType,iNoise)	=	0                                               ;
            else
                F2(iType,iNoise)	=	F1(iType,iNoise)/Ktrue1                         ;
            end
        end
        JJ(iType)	=	J	;
end

e	=	reshape( (1:nNoise)'*ones(1,nType), [nType*nNoise 1] )      ;
s	=	reshape( ((1:nType)'*ones(1,nNoise))', [nType*nNoise 1] )	;
if nNoise==1
    Xlabels	=	[   repmat([SetName '.'], [nType*nNoise 1]) num2str((s),'%02d')	] ;
else
    Xlabels =	[	repmat(SetName,[nType*nNoise 1]) ....
                    num2str(s(:)) ...
                    repmat('.',[nType*nNoise 1]) num2str(e(:))	]	;
end

figure(73),
if exist('FigPrep','file'), FigPrep; end
hold on

for sub=1:2
    switch sub
        case 1
            F1	=	F1'         ;
            F	=	F1(:)*100   ;
        case 2
            F2	=   F2'         ;
            F	=   F2(:)*100	;
    end
    subplot( 2, 1, sub ), hold on,
    if exist( 'AxisPrep', 'file' ), AxisPrep; end
    bar( F, 'k' )
    set( gca,	'Xlim', [0 nType*nNoise]+0.5,'Xtick',1:(nType*nNoise), ...
                'XTickLabelRotation', 90, ...
                'Xticklabels', Xlabels )
    set( gca, 'Tickdir', 'out')
    
    if sub==2
        xlabel( 'Data set [c.s]' )
        ylabel( {'Fraction of','non-noisy variance','per component [%]'} )
    else 
        ylabel( {'Fraction of','non-noisy variance','[%]'} )
    end
    
end

subplot( 2, 1, 1 ), hold on,
Ylim	=	get( gca, 'Ylim' )  ;
for iType=1:nType
    x	=	(iType-1)*nNoise+(1:nNoise) ;
    text( mean(x), Ylim(2), ...
            {num2str(Ktrue(iType)),['(' num2str(JJ(iType)) ')']},...
            'HorizontalAlignment','center',...
            'VerticalAlignment','bottom',...
            'FontSize',16)
end
set( gca, 'Xticklabel', [] )
if nNoise>1
    if exist( 'PlotVertical', 'file' )
        PlotVertical( (1:(nType-1))*nNoise+.5, 'k-', 'linewidth', 2 ); 
    end
    subplot( 2, 1, 2 ), hold on,
    if exist( 'PlotVertical', 'file' )
        PlotVertical( (1:(nType-1))*nNoise+.5, 'k-', 'linewidth', 2 ); 
    end
end

for s=1:2
    sub     =	subplot(2,1,s)      ;
    Pos     =	sub.Position        ;
    Pos(1)	=	0.1                 ;
    Pos(2)	=	Pos(2)+(s-1)*.05	;
    sub.Position	=   Pos         ;  
end

figurepath = [ path_figures 'fig_'  SetName  '_nonnoisy_' Subset   ];
saveas (gcf,figurepath,'fig')
saveas (gcf,figurepath,'epsc')
saveas (gcf,figurepath,'tiff')

disp('Figure non-noisy variance done ')

%% FIGURE 1
disp('Figure computational efficiency ... ')

% ------------------------------------------------------------------------
%	PLOT 1 - COMPUTATIONAL TIME
% ------------------------------------------------------------------------
TimeUsed2D  =	reshape(permute(TimeUsed,[2 1 3]),[nType*nNoise nMethod]) ;

TimeMaxLog10 = ceil(log10(max(TimeUsed2D(:))));
TimeMinLog10 = floor(log10(min(TimeUsed2D(:))));

e = reshape( (1:nNoise)'*ones(1,nType), [nType*nNoise 1]) ;
s = reshape( ((1:nType)'*ones(1,nNoise))', [nType*nNoise 1] ) ;
Marker = {'^','^','>','>','v','v','<','<','o','+'} ;

figure(1),
if exist('FigPrep','file'), FigPrep; end
hold on
%set(gca,'FontName','DejaVu Sans Mono')
for iMethod=ViewMethod
    
    iMethodclass = ceil(iMethod/4 );
    iMethodsub = mod(iMethod-1,4)+1;
    subplot(3,1,iMethodclass);
    hold on,
    
    ms=11;
    plot(TimeUsed2D(:,iMethod),'k.','Marker',markersub{iMethodsub},'MarkerSize',11,'LineWidth',2);
    
    ylabel({'Average','computing','time [s]'})
    set(gca,'Yscale','log','Ytick',10.^[TimeMinLog10:TimeMaxLog10],'Ylim',10.^[TimeMinLog10 TimeMaxLog10])
    set(gca,'Xlim',[0 nType*nNoise]+0.5,'Xtick',1:(nType*nNoise),'XTickLabelRotation',90,'Xticklabels',Xlabels) %,'Xticklabels',Xlabels,'XTickLabelRotation',90)
    set(gca,'Tickdir','out')
    if iMethodclass==3
        xlabel('Data set [c.s]')
    else
        set(gca,'Xticklabel',[])
    end
    
    iMethodNames = (iMethodclass-1)*4+1:min([(iMethodclass)*4,nMethod]) ;
    h=legend(MethodNames(iMethodNames,:),'AutoUpdate','Off','Location','NorthEast');
    Pos = h.Position ;
    Pos(1)=0.9;
    h.Position =Pos;
    Ytick = get(gca,'Ytick');
    if exist('AxisPrep','file'), AxisPrep; end
    set(gca,'Ytick',Ytick)

end
subplot(3,1,1), hold on,
Ylim = get(gca,'Ylim');
for iType=1:nType
    x = (iType-1)*nNoise+(1:nNoise);
    text(mean(x),Ylim(2),{num2str(Ktrue(iType)),['(' num2str(JJ(iType)) ')']},'HorizontalAlignment','center','VerticalAlignment','bottom','FontSize',16)
end
if nNoise>1
    if exist('PlotVertical','file'), PlotVertical((1:(nType-1))*nNoise+.5,'k-','linewidth',2); end
    subplot(3,1,2), hold on,
    if exist('PlotVertical','file'), PlotVertical((1:(nType-1))*nNoise+.5,'k-','linewidth',2); end
    subplot(3,1,3), hold on,
    if exist('PlotVertical','file'), PlotVertical((1:(nType-1))*nNoise+.5,'k-','linewidth',2); end
end
for s=1:3
    sub = subplot(3,1,s);
    Pos = sub.Position;
    Pos(1) = 0.125;
    Pos(2) = Pos(2)+(s-1)*.05 ;
    sub.Position =Pos;  
end

figurepath = [ path_figures 'fig_'  SetName  '_compute_' Subset   ];
saveas (gcf,figurepath,'fig')
saveas (gcf,figurepath,'epsc')
saveas (gcf,figurepath,'tiff')

disp('Figure computational efficiency done ')

%% FIGURE 2
disp('Figure accuracy ... ')

figure(2),
if exist('FigPrep','file'), FigPrep; end
hold on
%set(gca,'FontName','DejaVu Sans Mono')
for iMethod=ViewMethod
    
    iMethodclass = ceil(iMethod/4 );
    iMethodsub = mod(iMethod-1,4)+1;
    subplot(3,1,iMethodclass), hold on,
    if exist('AxisPrep','file'), AxisPrep; end
    
    ms=11;
    x_ = zeros(0,1);
    y_ = zeros(0,1);
    for iType=1 %:nType
        x = (iType-1)*nNoise+(1:nNoise);
        y=CountCorrect(iType,:,iMethod)/nRep*100 ;
        x_ = [ x_ x(:) ];
        y_ = [ y_ y(:) ];
    end
    if nNoise==1
        plot(x_,y_,'k.','Marker',markersub{iMethodsub},'MarkerSize',11,'LineWidth',2);
    else
        plot(x_,y_,'k.-','Marker',markersub{iMethodsub},'MarkerSize',11,'LineWidth',2);
    end
    
    iMethodNames = (iMethodclass-1)*4+1:min([(iMethodclass)*4,nMethod]) ;
    h=legend(MethodNames(iMethodNames,:),'AutoUpdate','Off','Location','NorthEast');
    Pos = h.Position ;
    Pos(1)=0.87;
    h.Position =Pos;
    
end

for iMethod=ViewMethod
    
    iMethodclass = ceil(iMethod/4 );
    iMethodsub = mod(iMethod-1,4)+1;
    subplot(3,1,iMethodclass), hold on,
    if exist('AxisPrep','file'), AxisPrep; end
    
    ms=11;
    x_ = zeros(0,1);
    y_ = zeros(0,1);
    for iType=1:nType
        x = (iType-1)*nNoise+(1:nNoise);
        y=CountCorrect(iType,:,iMethod)/nRep*100 ;
        x_ = [ x_ x(:) ];
        y_ = [ y_ y(:) ];
    end
    if nNoise==1
        plot(x_,y_,'k.','Marker',markersub{iMethodsub},'MarkerSize',11,'LineWidth',2);
    else
        plot(x_,y_,'k.-','Marker',markersub{iMethodsub},'MarkerSize',11,'LineWidth',2);
    end
    ylabel({'Accuracy [%]'})
    set(gca,'Ylim',[0 105],'Ytick',[0:10:100])
    set(gca,'Ygrid','on')
%     set(gca,'Yscale','log','Ytick',10.^[-3:1],'Ylim',[10^-3 10^1])
    set(gca,'Xlim',[0 nType*nNoise]+0.5,'Xtick',1:(nType*nNoise),'XTickLabelRotation',90,'Xticklabels',Xlabels) %,'Xticklabels',Xlabels,'XTickLabelRotation',90)
    set(gca,'Tickdir','out')
    if iMethodclass==3
        xlabel('Data set [c.s]')
    else
        set(gca,'Xticklabel',[])
    end
    
end
subplot(3,1,1), hold on,
Ylim = get(gca,'Ylim');
for iType=1:nType
    x = (iType-1)*nNoise+(1:nNoise);
    text(mean(x),Ylim(2),{num2str(Ktrue(iType)),['(' num2str(JJ(iType)) ')']},'HorizontalAlignment','center','VerticalAlignment','bottom','FontSize',16)
end
if nNoise>1
    if exist('PlotVertical','file'), PlotVertical((1:(nType-1))*nNoise+.5,'k-','linewidth',2); end
    subplot(3,1,2), hold on,
    if exist('PlotVertical','file'), PlotVertical((1:(nType-1))*nNoise+.5,'k-','linewidth',2); end
    subplot(3,1,3), hold on,
    if exist('PlotVertical','file'), PlotVertical((1:(nType-1))*nNoise+.5,'k-','linewidth',2); end
end

for s=1:3
    sub = subplot(3,1,s);
    Pos = sub.Position;
    Pos(1) = 0.1;
    Pos(2) = Pos(2)+(s-1)*.05 ;
    sub.Position =Pos;    
end

figurepath = [ path_figures 'fig_'  SetName  '_accuracy_' Subset   ];
saveas (gcf,figurepath,'fig')
saveas (gcf,figurepath,'epsc')
saveas (gcf,figurepath,'tiff')

disp('Figure accuracy done ')


%% FIGURE 3
disp('Figure overall accuracy ... ')

% ------------------------------------------------------------------------
%	PLOT 3 - OVERALL ACCURACY
% ------------------------------------------------------------------------

% CountCorrect2D  =	reshape(permute(CountCorrect,[2 1 3]),[nType*nNoise nMethod]) ;
if CASE==3
    
    figure(3), hold on
    if exist('FigPrep','file'), FigPrep; end
    set(gca,'FontName','DejaVu Sans Mono')
    if exist('AxisPrep','file'), AxisPrep; end
    
    y=CountCorrect(:,:,:)/nRep*100 ;
    y = squeeze(sum(y,2))/(nNoise);
    
    bar(y','k')
    set(gca,'Xtick',1:length(ViewMethod),'Xticklabel',MethodNamesCell(ViewMethod),'XTickLabelRotation',90)
    
    xlabel('Model selection criterion')
    ylabel({'Accuracy [%]'})
    set(gca,'Ylim',[0 105],'Ytick',[0:10:100])
    set(gca,'Ygrid','on')
    set(gca,'Tickdir','out')
    
else
    figure(3), hold on
    if exist('FigPrep','file'), FigPrep; end
    set(gca,'FontName','DejaVu Sans Mono')
    if exist('AxisPrep','file'), AxisPrep; end
    
    y=CountCorrect(:,:,:)/nRep*100 ;
    y = squeeze(sum(sum(y,1),2))/(nNoise*nType);
    
    bar(y,'k')
    set(gca,'Xtick',1:length(ViewMethod),'Xticklabel',MethodNamesCell(ViewMethod),'XTickLabelRotation',90)
    
    xlabel('Model selection criterion')
    ylabel({'Accuracy [%]'})
    set(gca,'Ylim',[0 105],'Ytick',[0:10:100])
    set(gca,'Ygrid','on')
    set(gca,'Tickdir','out')
    
end
drawnow()

figurepath = [ path_figures 'fig_' SetName '_overallaccuracy_' Subset  ];
saveas (gcf,figurepath,'fig')
saveas (gcf,figurepath,'epsc')
saveas (gcf,figurepath,'tiff')

disp('Figure overall accuracy done ')

%% FIGURE 4
disp('Figure distribution of estimated dimensionality ... ')

% ------------------------------------------------------------------------
%	PLOT 3 - DISTRIBUTION OF IDENTIFIED NUMBER OF PRINCIPAL COMPONENTS
% ------------------------------------------------------------------------

nViewMethod = length(ViewMethod);

if CASE==3
    
    for iType=1:nType
        figure(iType*10); hold on,
        if exist('FigPrep','file'), FigPrep; end
        if exist('AxisPrep','file'), AxisPrep; end
        drawnow()
        %Counts = squeeze(CountPC(iType,:,:,:));
        Counts = permute(CountPC(iType,:,ViewMethod,:),[2 3 4 1]) ;
        Counts = reshape(Counts,[nNoise*nViewMethod nOVmax+1]) ;
        x_ = 1:(nViewMethod*nNoise) ;
        y_ = 0:Kmax(iType) ;
        Counts = Counts(:,1:(Kmax(iType)+1) );
        [X,Y]=meshgrid(x_,y_);
        S= (Counts/nRep)';
        set(gca,'Ylim',[0 nViewMethod*nNoise]+.5)
        set(gca,'Xlim',[0 Kmax(iType)+1]-.5)
        %set(gca,'FontName','DejaVu Sans Mono')
        
        setnames = Xlabels((iType-1)*nNoise+(1:nNoise),:) ;
        ylabels = cell(1,1);
        for m=1:size(setnames,1)
            for n=1:nViewMethod
                if m==3
                    ylabels{n,m} = MethodNamesCell{ViewMethod(n)} ;
                else
                    ylabels{n,m} = ' ' ;
                end
            end
        end
        ylabels =ylabels';
        
        for m=1:length(x_)
            for n=1:length(y_)
                if S(n,m)>0
                    w = (S(n,m)).^(1/2)*.95;
                    pos = [ y_(n)-w/2 x_(m)-w/2  w w];
                    rectangle('Position',pos,'EdgeColor','k','FaceColor','k','LineStyle','none')
                end
            end
        end
        xlabel('# principal components (K)')
        %ylabel(['Criterion'],'Rotation',-90,'VerticalAlignment','top')
        ylabel(['Criterion / Noise level'])
        
        set(gca,'Ytick',[1:(nViewMethod*nNoise)],'Yticklabel',ylabels(:),'Ydir','reverse') %
        %set(gca,'XAxislocation','top','XTickLabelRotation',-90)
        %set(gca,'XAxislocation','top')
        
        Xlim = get(gca,'Xlim');
        Ylim = get(gca,'Ylim');
        AspectData = (Ylim(2)-Ylim(1))/(Xlim(2)-Xlim(1));
        PosGCA = get(gca,'Position');
        PosGCF = get(gcf,'Position');
        AspectWindow = PosGCA(4)/PosGCA(3)*PosGCF(4)/PosGCF(3);
        if AspectData > AspectWindow
            PosGCA(3) = PosGCA(3)/(AspectData/AspectWindow) ;
            Xtick = [0:3:Kmax(iType)];
            Xlabel = cellstr(num2str(Xtick')) ;
            set(gca,'Xtick',Xtick,'Xticklabel',Xlabel,'Xminortick','on')
            XAX=get(gca,'XAxis');
            set(XAX,'MinorTickValues',[0:1:Kmax(iType)]);
        else
            PosGCA(4) = PosGCA(4)*(AspectData/AspectWindow) ;
        end
        PosGCA(1) = PosGCA(1) +.05;
        set(gca,'Position',PosGCA)
        if exist('PlotHorizontal','file'), PlotHorizontal( (1:(nMethod-1))*nNoise +.5,'k-','linewidth',1); end
        if exist('PlotVertical','file'), PlotVertical(Ktrue(iType),':','linewidth',2,'Color','k'); end
        
        figurepath = [ path_figures 'fig_' SetName '_PCdist_Type' num2str(iType)  '_' Subset  ];
        saveas (gcf,figurepath,'fig')
        saveas (gcf,figurepath,'epsc')
        saveas (gcf,figurepath,'tiff')
        
        
    end
    
else
    
    for iType=1:nType
        figure(iType*10); hold on,
        if exist('FigPrep','file'), FigPrep; end
        if exist('AxisPrep','file'), AxisPrep; end
        drawnow()
        %Counts = squeeze(CountPC(iType,:,:,:));
        Counts = permute(CountPC(iType,:,ViewMethod,:),[3 2 4 1]) ;
        Counts = reshape(Counts,[nNoise*nViewMethod nOVmax+1]) ;
        x_ = 1:(nViewMethod*nNoise) ;
        y_ = 0:Kmax(iType) ;
        Counts = Counts(:,1:(Kmax(iType)+1) );
        [X,Y]=meshgrid(x_,y_);
        S= (Counts/nRep)';
        set(gca,'Ylim',[0 nViewMethod*nNoise]+.5)
        set(gca,'Xlim',[0 Kmax(iType)+1]-.5)
        %set(gca,'FontName','DejaVu Sans Mono')
        
        setnames = Xlabels((iType-1)*nNoise+(1:nNoise),:) ;
        ylabels = cell(1,1);
        for m=1:size(setnames,1)
            for n=1:nViewMethod
                ylabels{n,m} = [  MethodNames(ViewMethod(n),:) ];
            end
        end
        
        for m=1:length(x_)
            for n=1:length(y_)
                if S(n,m)>0
                    w = (S(n,m)).^(1/2)*.95;
                    pos = [ y_(n)-w/2 x_(m)-w/2  w w];
                    rectangle('Position',pos,'EdgeColor','k','FaceColor','k','LineStyle','none')
                end
            end
        end
        xlabel('# principal components (K)')
        %ylabel(['Criterion'],'Rotation',-90,'VerticalAlignment','top')
        ylabel(['Criterion'])
        
        set(gca,'Ytick',[1:(nViewMethod*nNoise)],'Yticklabel',ylabels(:),'Ydir','reverse') %
        %set(gca,'XAxislocation','top','XTickLabelRotation',-90)
        
        Xlim = get(gca,'Xlim');
        Ylim = get(gca,'Ylim');
        AspectData = (Ylim(2)-Ylim(1))/(Xlim(2)-Xlim(1));
        PosGCA = get(gca,'Position');
        PosGCF = get(gcf,'Position');
        AspectWindow = PosGCA(4)/PosGCA(3)*PosGCF(4)/PosGCF(3);
        if AspectData > AspectWindow
            PosGCA(3) = PosGCA(3)/(AspectData/AspectWindow) ;
            set(gca,'Xtick',[0:Kmax(iType)])
        else
            PosGCA(4) = PosGCA(4)*(AspectData/AspectWindow) ;
        end
        PosGCA(1) = PosGCA(1) +.05;
        set(gca,'Position',PosGCA)
        if exist('PlotHorizontal','file'), PlotHorizontal( (1:(nNoise-1))*nMethod +.5,'k-','linewidth',1); end
        if exist('PlotVertical','file'), PlotVertical(Ktrue(iType),':','linewidth',2,'Color','k'); end
        
        figurepath = [ path_figures 'fig_' SetName '_PCdist_Type' num2str(iType)  '_' Subset  ];
        saveas (gcf,figurepath,'fig')
        saveas (gcf,figurepath,'epsc')
        saveas (gcf,figurepath,'tiff')
        
    end
    
end

disp('Figure distribution of estimated dimensionality done ')

