
% -------------------------------------------------------------------------
% PCA dimensionality selection - script_figure_combination.m
%
% Script which produces figures for the manuscript [1]. 
%
% [1] https://arxiv.org/abs/1902.03293
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 21.05.2019
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Stefania Russo, Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

more off
clc
clear all
close all

openfig('.\result_figures\fig_B1_overallaccuracy_ALL.fig');
drawnow()
ax = get(gcf,'Children');
h=get(ax,'Children');
X(:,1) = h.XData;
Y(:,1) = h.YData;
openfig('.\result_figures\fig_B2_overallaccuracy_ALL.fig');
drawnow()
ax = get(gcf,'Children');
h=get(ax,'Children');
X(:,2) = h.XData;
Y(:,2) = h.YData;
openfig('.\result_figures\fig_C_overallaccuracy_ALL.fig');
drawnow()
ax = get(gcf,'Children');
h=get(ax,'Children');
for s=1:4
    X(:,s+2) = h(s).XData;
    Y(:,s+2) = h(s).YData;
end
Ylim = get(gca,'Ylim');
Yticklabel = get(gca,'Yticklabel');
Xlim = get(gca,'Xlim');
Xticklabel = get(gca,'Xticklabel');

%close all

% CountCorrect2D  =	reshape(permute(CountCorrect,[2 1 3]),[nType*nNoise nMethod]) ;
figure(11), hold on
if exist('FigPrep','file'), FigPrep; end
if exist('AxisPrep','file'), AxisPrep; end

colour = [	244,109,67 ;
            215,48,39 ;
            224,243,248 ;
            171,217,233 ;
            116,173,209 ;
            69,117,180 ]/255 ;

hb = bar(Y);
for p=1:length(hb)
    hb(p).FaceColor  = colour(p,:);
end
set(gca,'Xtick',1:length(Xticklabel),'Xticklabel',Xticklabel) %,'XTickLabelRotation',90)

xlabel('Model selection criterion')
ylabel({'Accuracy [%]'})
legend({'B1','B2','C1','C2','C3','C4'},'Location','NorthWest')
set(gca,'Ylim',[0 102],'Ytick',[0:5:100])
set(gca,'Ygrid','on')
set(gca,'Tickdir','out','XTickLabelRotation',90)

drawnow()

path_figures	= '.\result_figures\'       ;
figurepath = [ path_figures 'fig_CLASS_accuracy' ];
saveas (gcf,figurepath,'fig')
saveas (gcf,figurepath,'epsc')
saveas (gcf,figurepath,'tiff')

disp('Figure overall accuracy done ')