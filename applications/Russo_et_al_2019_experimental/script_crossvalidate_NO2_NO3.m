
% -------------------------------------------------------------------------
% PCA dimensionality selection - script_crossvalidate_NO2_NO3.m
%
% Script which tests dimensionality selection methods on the UV-Vis
% spectra collected for [1].
%
% [1] https://arxiv.org/abs/1902.03293
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 21.05.2019
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Stefania Russo, Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

more off
clc
clear all
close all

addpath('../../tools')

%% LABORATORY STUDY SETUP

GetMethods

path_local  =   erase( mfilename( 'fullpath' ), mfilename () );
path_out	=	[ path_local 'result_NO2_NO3\' ]	;
filedata	=	[ path_local 'experimentaldata_NO2_NO3\Spectra_t_origin_all.csv' ] ;

%% META-DATA
nMethod     =   size(Methods,1)     ;

%% STORE DESIGN
filepath        =   [ path_out  'design.csv' ]                      ;
% open file
fileID          =	fopen (filepath,'w')                            ;
% write header
formatLine      =   [ repmat('%s;',1,5) '\n'  ]                     ;
heads = {'model','centering','scaling','CV-pattern','criterion'}	;
fprintf (fileID, formatLine, heads{:} )                             ;
for iMethod=1:nMethod
    % write data
    out         =	Methods(iMethod,:)                              ;
    fprintf (fileID, formatLine, out{:} )                           ;
end
% close file
fclose(fileID);
drawnow()

%% READ DATA

datat	=	readtable(filedata)                                 ;
time	=	datenum(table2array(datat(:,1)),'dd.mm.yyyy HH:MM')	;
data	=	table2array(datat(:,2:end))                         ;
spectra	=	data(:,3:end)                                       ;

wavelen =	200:2.5:735                                         ;

% List index of media, each index corresponds to a unique combination of
% added volumes of NO2 and NO3 stock solution
medium	=	ones(5,1)*(1:256)	;
medium	=	medium(:)           ;

%% CROSS-VALIDATION BLOCK ASSIGNMENT

rng(11)
nBin	=	16                                          ;
latin	=	(toeplitz(1:(nBin+1), fliplr(1:(nBin+1))))	;
latin	=	fliplr(latin(1:nBin,1:nBin))                ;
latin	=	round(latin)                                ;

rr      =	randperm(nBin)	;
cc      =	randperm(nBin)	;
latin	=	latin(rr, cc)   ;
latin1	=	latin(:)        ;

blocks	=	ones(5,1)*latin1(:)'	;
blocks	=	blocks(:)               ;

% STORE ASSIGNMENT
filepath        =   [ path_out  'latin.csv' ]                       ;
% open file
fileID          =	fopen (filepath,'w')                            ;
% write header
formatLine      =   [ repmat('%d;',1,nBin) '\n'  ]                  ;
for iBin=1:nBin
    % write data
    out         =	latin(iBin,:)                              ;
    fprintf (fileID, formatLine, out  )                           ;
end
% close file
fclose(fileID);
drawnow()


%% LOOPS: ACTUAL BENCHMARKING
skip        =	1           ;
Ktrue       =   2           ;
Simulation	=	false       ;

% --------------------------------------------------------------------
%	LOOP 1: For every selection of wavelengths
% --------------------------------------------------------------------
for selection=1:2
    
    % Identify selected columns
    switch selection
        case 0
            cols    =   1:skip:length(wavelen);
        case 1
            cols    =   35:skip:length(wavelen);
        case 2
            cols    =   35:skip:75;
    end
    
    % --------------------------------------------------------------------
    %	LOOP 2: For original (0) or simulated (1/2) versions of data
    % --------------------------------------------------------------------
    
    for mod=0:2
        
        disp(['Selection:' num2str(selection) ' Simulate:' num2str(mod) ])
        Ytil    =	spectra(:,cols)     ;
        J       =	length(cols)        ;
        
        % ----------------------------------------------------------------
        %   Replace data with simulated version if necessary
        % ----------------------------------------------------------------
        if mod>0
            
            % fit PPCA model
            [mu,sigma,lambda2o,V,U]	=	fDecompose(Ytil,1,0)        ;
            [sigma2,phi2,lambda2]	=	fPPCAconstruct(lambda2o,Ktrue,J)	;
            
            if mod==2
                % sampled principal scores - randomly sample normalized scores
                U	=	randn(size(U)).*std(U(:))	;
            else % (mod==1)
                % keep principal scores as identified for experimental data
            end
            
            % simulated measurements
            Yt      =	U(:,1:Ktrue)*diag(phi2(1:Ktrue))*V(:,1:Ktrue)'+mu       ;
            Ytn     =	Yt+sigma2*randn(size(Yt))                   ;
            Ytil	=	Ytn                                         ;
        end
        
        % ----------------------------------------------------------------
        % SVD decomposition - for visualization purposes
        % ----------------------------------------------------------------
        mu          =	mean(Ytil)      ;
        [U,S,V]     =	svd(Ytil-mu)	;
        
        % Store loading vectors
        filepath        =   [ path_out  'Set'  num2str(selection)  '_Sim' num2str(mod) '_LoadingVectors.csv' ]                      ;
        
        % open file
        fileID          =	fopen (filepath,'w')                            ;
        
        % write header
        formatLine      =   [ repmat('%s;',1,J+1) '\n'  ];
        heads = cellstr([num2str(wavelen(cols)') repmat('nm',J,1)  ])' ;
        heads = [ {'PC'}  , heads ];
        fprintf (fileID, formatLine, heads{:} )                         ;
        
        % write data
        formatLine      = [  '%3d;' repmat('%12.6f;',1,J) '\n' ];
        for iPC=1:size(V,2)
            out = num2cell([ iPC ;  V(:,iPC) ]);
            fprintf (fileID, formatLine, out{:} )                         ;
        end
        
        % close file
        fclose(fileID);
        drawnow()
        
        % ----------------------------------------------------------------
        %   Execute cross-validation script 
        % ----------------------------------------------------------------
        
        J               =	size(Ytil,2);
        filenamebase	=  [ 'Set' num2str(selection) ];
            
        CrossValidate
        
    end
    
end

return