
% -------------------------------------------------------------------------
% PCA dimensionality selection - script_crossvalidate_CoII_CrIII_NiII.m
%
% Script which tests dimensionality selection methods on the
% Co(II)/Cr(III)/Ni(II) data set from [1].
%
% [1] Wentzell, P. D.; Lohnes, M. T. (1999). Maximum likelihood principal
% component analysis with correlated measurement errors: theoretical and
% practical considerations. Chemometrics and Intelligent Laboratory
% Systems, 45, 1999, 65�85. 
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 21.05.2019
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Stefania Russo, Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

clc
clear all
close all

addpath('../../tools')

%% LABORATORY STUDY SETUP

GetMethods

path_local  =   erase( mfilename( 'fullpath' ), mfilename () );
path_out	=	[ path_local 'result_CoII_CrIII_NiII\' ]	;
filedata	=	[ path_local 'experimentaldata_CoII_CrIII_NiII\data.csv' ] ;

addpath([path_local '../tools'])

which(mfilename('fullpath'))

%% META-DATA
nMethod     =   size(Methods,1)     ;

%% STORE DESIGN
filepath        =   [ path_out  'design.csv' ]                      ;
% open file
fileID          =	fopen (filepath,'w')                            ;
% write header
formatLine      =   [ repmat('%s;',1,5) '\n'  ]                     ;
heads = {'model','centering','scaling','CV-pattern','criterion'}	;
fprintf (fileID, formatLine, heads{:} )                             ;
for iMethod=1:nMethod
    % write data
    out         =	Methods(iMethod,:)                              ;
    fprintf (fileID, formatLine, out{:} )                           ;
end
% close file
fclose(fileID);
drawnow()

% READ DATA

datat	=	readtable(filedata) ;
data	=	table2array(datat)	;
conc	=	data(:,1:3)         ;
spectra	=	data(:,4:end)       ;

header=char(datat.Properties.VariableNames(4:end)') ; 
wavelen =	str2num(header(:,2:4))'                 ;

% conc = load(filedata,'CONC'); % Cr, Ni and Co
% 
% conc = conc.CONC;
% spectra = load(filedata,'DATA');
% spectra = spectra.DATA;
% wavelen = load(filedata,'WAV');
% wavelen = wavelen.WAV ;
% 
% header = cellstr([ num2str(wavelen(:)) repmat('nm',[length(wavelen) 1])]);
% header = [ 'Cr' ;'Ni' ;  'Co' ; header ];
% 
% size(header)
% Z = [ conc spectra];
% [I,J]=size(Z);
% Z = round(Z*10^20)/10^20;
% 
% 
% % STORE ASSIGNMENT
% filepath        =   [ path_out  'data.csv' ]                       ;
% % open file
% fileID          =	fopen (filepath,'w')                            ;
% % write header
% formatLine      =   [ repmat('%s;',1,J-1) '%s\n'  ]                  ;
% fprintf (fileID, formatLine, header{:}  )                           ;
% % write data
% formatLine      =   [ repmat('%0.42f;',1,J-1) '%0.42f\n'  ]                  ;
% for i=1:I
%     out         =	Z(i,:)                              ;
%     fprintf (fileID, formatLine, out  )                           ;
% end
% % close file
% fclose(fileID);
% drawnow()
% 
% return

M = size(conc,1) ;
suspect = [12 28] ;
inc  = setdiff(1:M,suspect);
conc = conc(inc,:);
spectra = spectra(inc,:);

%% CROSS-VALIDATION BLOCK ASSIGNMENT

rng(11)

design = nan(3,3,3) ;
design(:,:,1) = [ 1 2 3 ; 4 5 6 ; 7 8 9 ];
design(:,:,2) = [ 5 7 8 ; 9 3 1 ; 2 6 4 ];
design(:,:,3) = [ 6 4 7 ; 8 9 2 ; 3 1 5 ];

c1 = sort(unique(conc(:,1))) ;
c2 = sort(unique(conc(:,2))) ;
c3 = sort(unique(conc(:,3))) ;

nBlock = 9 ;
blocks = nan(size(spectra,1),1) ;
for iBlock = 1:nBlock
    [i_,j_,k_]=ind2sub([3 3 3],find(design(:)==iBlock));
    for h = 1:length(i_)
        combo = [ c1(i_(h)) c2(j_(h)) c3(k_(h)) ] ;
        xThisSet = ismember(conc,combo,'rows');
        if any(xThisSet)
            blocks(xThisSet) = iBlock ;
        end

    end
end

%% LOOPS: ACTUAL BENCHMARKING
skip	=	1           ;
Ktrue       =   3           ;
Simulation	=	false       ;

% --------------------------------------------------------------------
%	LOOP 1: For every selection of wavelengths
% --------------------------------------------------------------------
for selection=0
    
    % Identify selected columns
    switch selection
        case 0
            cols    =   1:skip:length(wavelen);
        case 1
            cols    =   41:skip:141;
        case 2
            cols    =   35:skip:75;
    end

    % --------------------------------------------------------------------
    %	LOOP 2: For original (0) / simulated (1) version of data
    % --------------------------------------------------------------------
    
    for mod=0:2
        
        disp(['Selection:' num2str(selection) ' Simulate:' num2str(mod) ])
        Ytil = spectra(:,cols) ;
        J   = length(cols) ;
        
        if mod>0
            
            % fit PPCA model
            [mu,sigma,lambda2o,V,U]	=	fDecompose(Ytil,1,0)        ;
            [sigma2,phi2,lambda2]	=	fPPCAconstruct(lambda2o,Ktrue,J)	;
            
            if mod==2
                % sampled principal scores - randomly sample normalized scores
                U	=	randn(size(U)).*std(U(:))	;
            else % (mod==1)
                % keep principal scores as identified for experimental data
            end
            
            % simulated measurements
            Yt      =	U(:,1:Ktrue)*diag(phi2(1:Ktrue))*V(:,1:Ktrue)'+mu       ;
            Ytn     =	Yt+sigma2*randn(size(Yt))                   ;
            Ytil	=	Ytn                                         ;
        end
        
        % ----------------------------------------------------------------
        % SVD decomposition - for visualization purposes
        mu          =	mean(Ytil)      ;
        [U,S,V]     =	svd(Ytil-mu)	;
        
        % Store loading vectors
        filepath        =   [ path_out  'Set'  num2str(selection)  '_Sim' num2str(mod) '_LoadingVectors.csv' ]                      ;
        
        % open file
        fileID          =	fopen (filepath,'w')                            ;
        
        % write header
        formatLine      =   [ repmat('%s;',1,J+1) '\n'  ];
        heads = cellstr([num2str(wavelen(cols)') repmat('nm',J,1)  ])' ;
        heads = [ {'PC'}  , heads ];
        fprintf (fileID, formatLine, heads{:} )                         ;
        
        % write data
        formatLine      = [  '%3d;' repmat('%12.6f;',1,J) '\n' ];
        for iPC=1:size(V,2)
            out = num2cell([ iPC ;  V(:,iPC) ]);
            fprintf (fileID, formatLine, out{:} )                         ;
        end
        
        % close file
        fclose(fileID);
        drawnow()
        
        % ----------------------------------------------------------------
        
        % META-DATA
        J               =	size(Ytil,2);
        filenamebase    =  [ 'Set' num2str(selection) ];
        CrossValidate
        
        
    end
    
end

return