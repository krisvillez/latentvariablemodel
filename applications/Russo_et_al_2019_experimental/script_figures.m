
% -------------------------------------------------------------------------
% PCA dimensionality selection - script_figures.m
%
% Script which produces the figures for manuscript [1].
%
% [1] https://arxiv.org/abs/1902.03293
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 21.05.2019
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Stefania Russo, Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

more off
clc
clear all
close all

addpath('../../tools')
try
    addpath('C:\G\code\spike_b')
catch
end

warning('off','MATLAB:table:ModifiedAndSavedVarnames')

for CASE=1 %:2
    if CASE==1
        SetName = 'NO2_NO3';
        path_figures	= '.\result_NO2_NO3_figures\'       ;
        path_in         = '.\experimentaldata_NO2_NO3\'     ;
        path_out        = '.\result_NO2_NO3_R2019a\'        ; % remove '_as_published' to use new results
        filedata        =	[path_in 'Spectra_t_origin_all.csv' ];
        nSelect         =   2;
        SELECT = 1:2 ;
        Kmax = [180 40 ];
    else
        SetName = 'CoII_CrIII_NiII';
        path_figures	= '.\result_CoII_CrIII_NiII_figures\'       ;
        path_in         = '.\experimentaldata_CoII_CrIII_NiII\'     ;
        path_out        = '.\result_CoII_CrIII_NiII_R2017b\'        ; % remove '_as_published' to use new results
        nSelect=1;
        SELECT = 0;
        Kmax = 111;
    end
    
    %% READ DESIGN
    disp('Loading design ...')
    filepath = [ path_out 'design.csv' ] ;
    tab     =	readtable(filepath,'Delimiter',';','HeaderLines',0)	;
    tab     =	tab(:,1:end-1)	;
    Methods =	table2cell(tab)	;
    
    MethodNames = upper(Methods(:,5));
    MethodNames{9} = [ MethodNames{9} ' (EKF)' ];
    MethodNames{10} = [ MethodNames{10} ' (RKF)' ];
    MethodNames =char(MethodNames);
    MethodNames(5:8,1)='c' ;
    
    % META-DATA
    nMethod     =   size(Methods,1)      ;
    
    disp('Loading design done ')
    
    if CASE==1
        
        disp('Loading latin square ...')
        filepath = [ path_out 'latin.csv' ] ;
        tab     =	readtable(filepath,'Delimiter',';','HeaderLines',0)	;
        tab     =	tab(:,1:end-1)	;
        Latin	=	table2array(tab)	;
        disp('Loading latin square done ')
        
        
        %% READ DATA
        
        datat	=	readtable(filedata)                                 ;
        time	=	datenum(table2array(datat(:,1)),'dd.mm.yyyy HH:MM')	;
        data	=	table2array(datat(:,2:end))                         ;
        spectra	=	data(:,3:end)                                       ;
        
        wavelen =	200:2.5:735                                         ;
        
        % List index of media, each index corresponds to a unique combination of
        % added volumes of NO2 and NO3 stock solution
        medium	=	ones(5,1)*(1:256)	;
        medium	=	medium(:)           ;
        
        
        %% FIGURE 1
        disp('Figure latin square ... ')
        
        % ------------------------------------------------------------------------
        %	PLOT 1 - LATIN SQUARE
        % ------------------------------------------------------------------------
        
        nBin = size(Latin,1);
        volumes = 2*(1:nBin) ;
        [vNO3,vNO2] = meshgrid(volumes,volumes) ;
        vNO2 =vNO2(:);
        vNO3 =vNO3(:);
        Latin1 =Latin(:);
        
        figure(CASE*1000+1), hold on
        if exist('FigPrep','file'), FigPrep; end
        if exist('AxisPrep','file'), AxisPrep; end
        cmap = gray(16) ;
        
        for b = unique(Latin1(:))'
            index = find(Latin1 == b);
            han = plot(vNO2(index),vNO3(index),'ko','MarkerSize',15);
            han.MarkerFaceColor = cmap(b,:) ;
        end
        set(gca,'Xlim',[0 33],'Ylim',[0 33])
        legend(num2str((1:16)'),'Location','EastOutside')
        xlabel('Added volume of nitrite (NO_2^-) stock solution [mL] ')
        ylabel('Added volume of nitrate (NO_3^-) stock solution [mL]')
        
        figurepath = [ path_figures 'fig_' SetName '_grid' ];
        saveas (gcf,figurepath,'fig')
        saveas (gcf,figurepath,'epsc')
        saveas (gcf,figurepath,'tiff')
        
        disp('Figure latin square done ')
        
        
        %% FIGURE 2
        disp('Figure spectra ... ')
        
        % ------------------------------------------------------------------------
        %	PLOT 2 - SPECTRA
        % ------------------------------------------------------------------------
        
        medium_select = medium(Latin1==1,:);
        
        figure(CASE*1000+2), hold on
        if exist('FigPrep','file'), FigPrep; end
        if exist('AxisPrep','file'), AxisPrep; end
        rectangle('Position',[257 100 740-257 999-100 ],'linewidth',1)
        ind = (unique(medium_select)') ;
        B= length(ind) ;
        colours = nan(B,3);
        for b=1:B
            han1=plot(wavelen,spectra(medium==ind(b),:)','-');
            colours(b,:) = han1(1).Color ;
            for j=1:length(han1)
                han1(j).Color =colours(b,:) ;
            end
            
            col=25;
            han2=plot(wavelen(col:end),spectra(medium==ind(b),col:end)'*20+50,'-');
            for j=1:length(han2)
                han2(j).Color =colours(b,:) ;
            end
        end
        set(gca,'Xlim',[200 740])
        xlabel('Wavelength [nm]')
        ylabel('Absorbance [m^{-1}]')
        
        set(gca,'Xtick',[200:20:750],'Xminortick','on')
        XAX=get(gca,'XAxis');
        set(XAX,'MinorTickValues',[200:2.5:750]);
        
        ax1=gca;
        ax2 = axes('Position',get(ax1,'Position'),...
            'XAxisLocation','bottom',...
            'YAxisLocation','right',...
            'Color','none',...
            'XColor','k',...
            'YColor','k',...
            'XLim',get(ax1,'Xlim'),...
            'YLim',get(ax1,'Ylim'),...
            'YTick',[100:100:1000],...
            'YTicklabel',([100:100:1000]-100)/20,...
            'XTick',[],'XTickLabel',[]);
        if exist('AxisPrep','file'), AxisPrep; end
        ylabel('Absorbance [m^{-1}]')
        
        figurepath = [ path_figures 'fig_' SetName '_spectra' ];
        saveas (gcf,figurepath,'fig')
        saveas (gcf,figurepath,'epsc')
        saveas (gcf,figurepath,'tiff')
        
        disp('Figure spectra done ')
    end
    
    %% FIGURE 3+
    disp('Figure cross-validation results ... ')
    
    markersub = {'<','>','o','v'};
    
    colour = {	[0,0,0]/255,...
                [0,123,255]/255,...
                [0,123,255]/255,...
                [202,0,32]/255};

    % --------------------------------------------------------------------
    %	LOOP 1: For every selection of wavelengths
    % --------------------------------------------------------------------
    for selection=SELECT
        
        % --------------------------------------------------------------------
        %	Cross-validation figure
        % --------------------------------------------------------------------
        
        for row=1:2
            
            figure(CASE*1000+(selection+1)*100+(row-1)*10+1),
            if exist('FigPrep','file'), FigPrep; end
            
            for iMethod=1:nMethod
                
                iMethodclass = ceil(iMethod/4 );
                iMethodsub = mod(iMethod-1,4)+1;
                
                method              =   Methods(iMethod,:)	;
                model               =   method{1}           ;
                centering           =   method{2}           ;
                scaling             =   method{3}           ;
                method_cv           =   method{4}           ;
                method_estimation   =   method{5}           ;
                
                MethodName          =	[ 'Model' model '-' centering '-' scaling '_CV' method_cv '-' method_estimation ];
                filepath            =   [ path_out 'Set' num2str(selection) '_' MethodName  '.csv' ] ;
                
                tab = readtable(filepath);
                tab = tab(:,1:end-1);
                tab = table2array(tab);
                
                Ktrue       =	find(tab(1,3:end))-1        ;
                
                simulation = tab(2:end,1)   ;
                
                criterion	=	tab(2:end,3:end)            ;
                K_ = (1:size(criterion,2))-1;
                
                crit = criterion(row,:) ;
                simul = simulation(row);
                
                [~,index] =  min(crit);
                
                subplot(3,1,iMethodclass), hold on,
                if exist('AxisPrep','file'), AxisPrep; end
                
                if mod(iMethod,2)==0
                    lines = ':' ;
                else
                    lines = '-' ;
                end
                plot(-11-K_,crit,lines,'LineWidth',2,'Marker',markersub{iMethodsub},'MarkerSize',11,'Color',colour{iMethodsub})
                
                iMethodNames = (iMethodclass-1)*4+1:min([(iMethodclass)*4,nMethod]) ;
                legend(MethodNames(iMethodNames,:),'AutoUpdate','Off','Location','North')
                grid on
            end
            
            for iMethod=1:nMethod
                
                iMethodclass = ceil(iMethod/4 );
                iMethodsub = mod(iMethod-1,4)+1;
                
                method              =   Methods(iMethod,:)	;
                model               =   method{1}           ;
                centering           =   method{2}           ;
                scaling             =   method{3}           ;
                method_cv           =   method{4}           ;
                method_estimation   =   method{5}           ;
                
                MethodName          =	[ 'Model' model '-' centering '-' scaling '_CV' method_cv '-' method_estimation ];
                filepath            =   [ path_out 'Set' num2str(selection) '_' MethodName  '.csv' ] ;
                
                tab = readtable(filepath);
                tab = tab(:,1:end-1);
                tab = table2array(tab);
                
                Ktrue       =	find(tab(1,3:end))-1        ;
                
                simulation = tab(2:end,1)   ;
                
                criterion	=	tab(2:end,3:end)            ;
                if CASE==1
                    if selection==1
                        q=1;
                    else
                        q=2;
                    end
                else
                    q = 1;
                end
                crit = criterion(row,1:Kmax(q)) ;
                simul = simulation(row);
                K_ = (1:length(crit))-1;
                
                [~,index] =  min(crit);

                
                subplot(3,1,iMethodclass), hold on,
                if exist('AxisPrep','file'), AxisPrep; end
                
                if mod(iMethod,2)==0
                    lines = ':' ;
                else
                    lines = '-' ;
                end
                plot(K_,crit,lines,'LineWidth',3,'Color',colour{iMethodsub})
                plot(K_(index),crit(index),'k','Marker',markersub{iMethodsub},'MarkerSize',17,'LineWidth',2,'Color',colour{iMethodsub})
                
                if CASE==2
                    set(gca,'Xlim',[0 Kmax(q)],'Xtick',[0:5:Kmax(q)],'Xminortick','on')
                else
                    if selection==1
                        set(gca,'Xlim',[0 Kmax(q)],'Xtick',[0:10:Kmax(q)],'Xminortick','on')
                    else
                        set(gca,'Xlim',[0 Kmax(q)],'Xtick',[0:5:Kmax(q)],'Xminortick','on')
                    end
                end
                XAX=get(gca,'XAxis');
                set(XAX,'MinorTickValues',[0:1:Kmax(q)]);
            
                if iMethodclass==3
                    if CASE==1
                        set(gca,'Ylim',[-3 +5])
                    else
                        set(gca,'Ylim',[-4 +2])
                    end
                    ylbl = {'IGN','VRE'} ;
                else
                    if CASE==2
                        if row==1
                            set(gca,'Ylim',[0 0.055])
                        else
                            set(gca,'Ylim',[0 0.00022])
                        end
                    else
                        set(gca,'Ylim',[0 5])
                    end
                    ylbl = {'RMSR'} ;
                end
                grid on
                
                ylabel(ylbl)
                if iMethod==nMethod
                    xlabel('# principal components [K]')
                elseif iMethodclass<3
                    set(gca,'Xticklabel',[])
                end
                grid on
                drawnow()
            end
            
            figurepath = [ path_figures 'fig_' SetName '_Set' num2str(selection) '_Sim' num2str(simul) '_profiles' ];
            
            saveas (gcf,figurepath,'fig')
            saveas (gcf,figurepath,'epsc')
            saveas (gcf,figurepath,'tiff')
        end
        
        % --------------------------------------------------------------------
        %	Loading vectors
        % --------------------------------------------------------------------
        
        modif         =       0       ;
        filepath    =   [	path_out  'Set'  num2str(selection)  '_Sim' num2str(modif) '_LoadingVectors.csv' ]	;
        
        tab         =	readtable(filepath)     ;
        loading     =	tab(:,2:end-1)          ;
        V           =	table2array(loading)'	;
        
        if CASE==2
            str         =	char(loading.Properties.VariableNames(:));
            wavelen     =	str2num(str(:,2:4))     ;
        else
            str         =	char(loading.Properties.VariableNames(:));
            wavelen     =	str2num(str(:,2:4))     ;
            wavelen = ceil(wavelen/2.5)*2.5;
        end
        
        figure(CASE*1000+(selection+1)*100+2),
        hold on
        if exist('FigPrep','file'), FigPrep; end
        if exist('AxisPrep','file'), AxisPrep; end
        plot(wavelen,V(:,Ktrue+1),'ko-','MarkerFaceColor','w','LineWidth',2,'MarkerSize',8)
        plot(wavelen,V(:,Ktrue+2),'k^-','MarkerFaceColor','c','LineWidth',2,'MarkerSize',8)
        plot(wavelen,V(:,Ktrue+3),'kv-','MarkerFaceColor','r','LineWidth',2,'MarkerSize',8)
        
        if CASE==2
            set(gca,'Xlim',[wavelen(1) wavelen(end)],'Xtick',[wavelen(1):20:wavelen(end)],'Xminortick','on')
        else
            set(gca,'Xlim',[wavelen(1) wavelen(end)],'Xtick',[300:20:750],'Xminortick','on')
        end
        XAX=get(gca,'XAxis');
        set(XAX,'MinorTickValues',[wavelen]);
        
        legend({['PC' num2str(Ktrue+1)],['PC' num2str(Ktrue+2)],['PC' num2str(Ktrue+3)]},'Location','NorthWest')
        xlabel('Wavelength [nm]')
        ylabel('Absorbance [m^{-1}]')
        drawnow()
        figurepath = [ path_figures 'fig_' SetName '_Set' num2str(selection) '_Sim' num2str(modif)  '_PC3to5' ];
        saveas (gcf,figurepath,'fig')
        saveas (gcf,figurepath,'epsc')
        saveas (gcf,figurepath,'tiff')
        
    end
    
    disp('Figure cross-validation results done ')
    
end
