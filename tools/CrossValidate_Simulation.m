
%% META-DATA
nType       =   length(Types)       ;
nNoise      =   length(NoiseLevels)	;
nMethod     =   size(Methods,1)     ;

blocks      =   (1:nBlock)'*ones(1,nSample);
blocks      =   blocks(:)           ;
blocks      =   blocks(1:nSample)   ;

%% STORE DESIGN
filepath        =   [ path_out  'design.csv' ] ;
% open file
fileID          =	fopen (filepath,'w')                            ;
% write header
fprintf (fileID, '%s;%d;\n', 'Blocks',nBlock )                         ;
fprintf (fileID, '%s;%d;\n', 'maxOV',J )                         ;
fprintf (fileID, '%s;%d;\n', 'Repetitions',nRep )                         ;
fprintf (fileID, '%s;%d;\n', 'Samples',nSample )                         ;
fprintf (fileID, '%s;%s;\n', 'SetName',SetName )                         ;
cs = cellstr([  num2str(Types(:)) ])' ;
fprintf (fileID, ['%s;' repmat('%s;',[1 nType]) '\n'],'Types',cs{:} )                         ;
cs = cellstr([  num2str(NoiseLevels(:)) ])' ;
fprintf (fileID, ['%s;' repmat('%s;',[1 nNoise]) '\n'],'NoiseLevels',cs{:} )                         ;
%fprintf (fileID, '%d', nRep )                         ;
% write header
formatLine      =   [ repmat('%s;',1,5) '\n'  ];
heads = {'model','centering','scaling','CV-pattern','criterion'} ;
fprintf (fileID, formatLine, heads{:} )                         ;
for iMethod=1:nMethod
    % write data
    out = Methods(iMethod,:);
    fprintf (fileID, formatLine, out{:} )                         ;
end
% close file
fclose(fileID);
drawnow()

%% LOOPS: ACTUAL BENCHMARKING

% --------------------------------------------------------------------
%	LOOP 1: For every repetition
% --------------------------------------------------------------------
for iRep=[0:nRep]
    
    disp(['Repetition: ' num2str(iRep) ' of ' num2str(nRep) ])
    
    seed    = iRep  ;
    rng(seed)
    
    TimeUsed        = nan(nType,nNoise,nMethod) ;
    
    W = randn(nSample,J) ;
    
    % --------------------------------------------------------------------
    %	LOOP 2/3: For every considered data set type and noise level
    % --------------------------------------------------------------------
    for iType =1:nType
        for iNoise=1:nNoise
            
            disp(['	Data set class: ' num2str(SetName) ' type: ' num2str(iType) ' of ' num2str(nType) ', Noise level: ' num2str(iNoise) ' of ' num2str(nNoise) ])
            
            % Data generation:
            setname     =   SetName ;
            type        =	Types(iType)	;   % set type
            noiselevel        =	NoiseLevels(iNoise)	;   % set type
            
            [Sigma2,nOVsim,Ktrue]	=	fGenerateCovariance(setname,type,noiselevel) ; % currently complete: A,B1,B2,C
            [U,S,V]=svd(Sigma2);
            lambda = (diag(S).^(1/2));
            Ytil                =   W(:,1:nOVsim)*diag(lambda)*V'         ;
            %PC                  =	(1:nOV)-1           ;
            
            filenamebase =  ['Set' SetName '.' num2str(iType,'%02d') '.' num2str(iNoise) ];
            
            Simulation = true ;
            
            CrossValidate
            
            
        end
    end
    
end