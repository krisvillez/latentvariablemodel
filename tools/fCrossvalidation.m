function [skill_mu,skill_sigma,skill_mu_sigma,skill]=fCrossvalidation(Ytil,model,centering,scaling,pattern,method,blockindex)

% -------------------------------------------------------------------------
% PCA dimensionality selection - fCrossvalidation.m
%
% Execute a cross-validation procedure for dimensionality selection.
%
% Syntax: 	[skill_mu,skill_sigma,skill_mu_sigma,skill]=fCrossvalidation(Ytil,model,centering,scaling,pattern,method,blockindex,originindex)
%
%	Inputs: 	
%       Ytil        IxJ data matrix with I samples, J variables
%       model       Model type (currently supported: 'pca',
%                   'ppca','blockppca','kppca','efa','efa1','efa2')
%       center      Centering method ('mean' or 'no')
%       scaling     Scaling method ('unitvar' or 'no')
%       pattern     Cross-validation pattern ('ekf' or 'rkf')
%       method      Cross-validation criterion/skill
%                   With pca+ekf: 'itr','pmp','tri','tsr','citr','cpmp','ctri', or 'ctsr'
%                   With pca+rkf: 'msr'
%                   With ppca+ekf: 'ign' or 'crps'
%                   With ppca+rkf: 'ign' or 'ignres'
%       blockindex  [optional] Column vector with integer index indicating
%                   the block of replicates the sample belongs too, useful
%                   to constrain the latent variable scores to be the same for replicates
%
%	Outputs: 	
%		skill_mu        Mean skill  
%       skill_sigma     Standard deviation (SD) of skill
%       skill_mu_sigma  Mean + 1 SD skill
%       skill           3D array of predictive skill scores:
%                       With ekf: I x 1 x Kmax (samples, variables, number of PCs)
%                       With rkf: I x J x Kmax (samples, -, number of PCs)
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 21.05.2019
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Stefania Russo, Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

global sigmak

% number of samples, number of observed variables
[nSample,J]	=	size(Ytil)          ;

% blockindex
if nargin<7 || isempty(blockindex)
    blockindex	=	(1:nSample)'        ;
else
    blockindex  =   blockindex(:)       ;
    assert(length(blockindex)==nSample,'Dimensions of "blockindex" do not match row size of "Ytil".')
end
blocks          =   unique(blockindex)  ;
nBlock          =   length(blocks)      ;

% % blockindex
% if nargin<8 || isempty(originindex)
%     originindex	=	(1:nSample)'        ;
% else
%     originindex  =   originindex(:)       ;
%     assert(length(originindex)==nSample,'Dimensions of "originindex" do not match row size of "Ytil".')
% end
% origins         =   unique(originindex)  ;
% nOrigin          =   length(origins)      ;

nSampleCalMin = length(blockindex)-max(histc(blockindex,unique(blockindex))) ;

% compute maximum number of PCs, assuming Leave-One-Sample-Out scheme
switch model
    case 'kppca'
        Kmax = ceil(nSampleCalMin/2);
    otherwise
        Kmax          = min(nSampleCalMin-2,J-1)  ; % apply -2 since (1) calibration data set has one sample less in LOOCV scheme and (2) account for mean estimate
        if strcmp(model(1:3),'efa')
            df2 = @(a,n) ((a-J)^2 - (a+J));
            while df2(Kmax,nSample-1)<0
                Kmax = Kmax-1;
            end
        end
end


% select model structure:
switch model
    case 'pca'
        % selected structure: principal component analysis (PCA)
        %disp('      Cross-validation PCA model')
        
        % ********************************************************************************
        % Prellocation
        % ********************************************************************************
        switch pattern % select cross-validation pattern
            % supported corss-validation patterns
            %   rkf: row-wise k-fold (implemented as as leave-one-out)
            %   ekf: element-wise k-fold (implemented as leave-one-out)
            %   cekf: corrected element-wise k-fold (implemented as leave-one-out)
            case 'rkf' 
                % only mean square residual criterion is supported:
                PermittedSkills =	{'msr'}                 ; 
                % pre-allocate matrix of results:
                skill           =	zeros(nSample,1,Kmax+1) ; 
            case 'ekf'
                % supported criteria:
                %   c/itr: Iterative imputation
                %   c/pmp: Projection to model plane
                %   c/tri: Trimmed score imputation
                %   c/tsr: Trimmed score regression
                PermittedSkills = {'itr','pmp','tri','tsr','citr','cpmp','ctri','ctsr'};
                skill   = zeros(nSample,J,Kmax+1) ;
            otherwise
                disp(['         CV method (' pattern ') not supported for model type (' model ').'])
        end
        
        % making sure the selected criterion is available
        assert(ismember(method,PermittedSkills),['Skill (' method ') not supported for cross-validation type (' pattern ').'])
        
        % ********************************************************************************
        % Actual cross-validation loop
        % ********************************************************************************
        
        % FOR EVERY BLOCK:
        for iBlock=1:nBlock
            
            %disp(['	Block ' num2str(iBlock) ' of ' num2str(nBlock)])
            
            % select the rows with validation data
            rowVal  =   find(blockindex==blocks(iBlock))    ;
            % select the rows with calibration data
            rowCal  =	setdiff(1:nSample,rowVal)           ;
            
            % =================================
            %   MATRIX DECOMPOSITION
            % =================================
            
            % compute mean and subtract it (if needed) and decompose with
            % SVD:
            [mu,sigma,lambda2o,Vo]= fDecompose(Ytil(rowCal,:),centering,scaling) ;
            
            % FOR EVERY NUMBER OF PRINCIPAL COMPONENTS:
            for K=Kmax:-1:0
                % =================================
                %   PCA MODEL SETUP
                % =================================
                
                % Compute/select PCA model elements,
                %   i.e. covariance matrix eigenvectors and eigenvalues 
                [V,lambda2]=fPCAconstruct(lambda2o,K,Vo) ;
                
                % =================================
                %   PPCA VALIDATION
                % =================================
                switch pattern
                    case 'rkf'
                        % compute the mean squared residual for the
                        % left-out sample
                        err2 = fPCAerror2(Ytil(rowVal,:),mu,sigma,lambda2,V,J) ;
                        skill(rowVal,1,K+1) = err2 ;
                    case 'ekf'
                        
                        switch method
                            case {'ctsr','citr','cpmp','ctri'}
                                % ------------------------------------------------
                                % Computed augmented matrix
                                % ------------------------------------------------
                                Y     =   (Ytil-mu)./sigma ;
                                T     =   Y*V ;
                                Yaug  = [ Y T ] ;
                                
                                % ------------------------------------------------
                                % Build augmented PCA model with calibration data
                                % ------------------------------------------------
                                [mu_aug,sigma_aug,lambda2o_aug,Vo_aug]= fDecompose(Yaug(rowCal,:),centering,0) ;
                                [V_aug,lambda2_aug]=fPCAconstruct(lambda2o_aug,K,Vo_aug) ;
                                
                            case {'tsr','itr','pmp','tri'}
                            otherwise
                                error("Unknown imputation method")
                        end
                        
                        switch method
                            case 'tsr'
                                % FOR EVERY OBSERVED VARIABLE:
                                for jMissing=1:J
                                    % -------------------------------------
                                    % Build regression model with
                                    % calibration data set
                                    % -------------------------------------
                                    Ytag = (Ytil(rowCal,:)-mu)./sigma ;
                                    if isempty(lambda2)
                                        B = [];
                                    else
                                        T = Ytag*V ;
                                        Ytag(:,jMissing) = 0;
                                        Ttag = Ytag*V ;
                                        B = (Ttag'*Ttag)\Ttag'*T ;
                                    end
                                    
                                    % -------------------------------------
                                    % Leave the considered variable out, as
                                    % if it is missing, and apply the
                                    % regression method to estimate it.
                                    % Then compute and report squared
                                    % residual between left-out variable
                                    % and its estimate.
                                    % -------------------------------------
                                    skill(rowVal,jMissing,K+1) = fPCAerror2impute(Ytil(rowVal,:),mu,sigma,lambda2,V,J,jMissing,method,B) ;
                                    
                                end
                            case {'itr','pmp','tri'}
                                % FOR EVERY OBSERVED VARIABLE
                                for jMissing=1:J
                                    % -------------------------------------
                                    % Leave the considered variable out, as
                                    % if it is missing, and apply the
                                    % imputation method to estimate it.
                                    % Then compute and report squared
                                    % residual between left-out variable
                                    % and its estimate.
                                    % -------------------------------------
                                    skill(rowVal,jMissing,K+1) = fPCAerror2impute(Ytil(rowVal,:),mu,sigma,lambda2,V,J,jMissing,method) ;
                                end
                            case 'ctsr'
                                
                                for jMissing=1:J
                                    % -------------------------------------
                                    % Build regression model with
                                    % calibration data set
                                    % -------------------------------------
                                    Ytag = Yaug(rowCal,:)-[mu zeros(1,K)] ;
                                    if length(lambda2)==0
                                        B = [];
                                    else
                                        T = Ytag*V_aug ;
                                        Ytag(:,jMissing) = 0;
                                        Ttag = Ytag*V_aug ;
                                        B = (Ttag'*Ttag)^(-1)*Ttag'*T ;
                                    end
                                    
                                    % -------------------------------------
                                    % Leave the considered variable out, as
                                    % if it is missing, and apply the
                                    % regression method to estimate it.
                                    % Then compute and report squared
                                    % residual between left-out variable
                                    % and its estimate.
                                    % -------------------------------------
                                    skill(rowVal,jMissing,K+1) = fPCAerror2impute(Yaug(rowVal,:),mu_aug,sigma_aug,lambda2,V_aug,J+K,jMissing,method,B) ;
                                    
                                end
                            case {'citr','cpmp','ctri'}
                                for jMissing=1:J
                                    % -------------------------------------
                                    % Leave the considered variable out, as
                                    % if it is missing, and apply the
                                    % imputation method to estimate it.
                                    % Then compute and report squared
                                    % residual between left-out variable
                                    % and its estimate.
                                    % -------------------------------------
                                    skill(rowVal,jMissing,K+1) = fPCAerror2impute(Yaug(rowVal,:),mu_aug,sigma_aug,lambda2_aug,V_aug,J+K,jMissing,method) ;
                                end
                            otherwise
                                disp('         Unknown imputation method')
                        end
                end
                
            end
        end
        
    case {'ppca','blockppca','kppca'}
        
        %disp('      Cross-validation PPCA model')
        % ********************************************************************************
        % Prellocation
        % ********************************************************************************
        switch pattern
            case 'rkf'
                PermittedSkills =	{'ign','ignres'}            ;
                skill           =	inf(nSample,1,Kmax+1,2)	;
            case 'ekf'
                PermittedSkills =	{'crps','ign','msr'}        ;
                skill           =	inf(nSample,J,Kmax+1,3) ;
            otherwise
                disp(['         CV method (' pattern ') not supported for model type (' model ').'])
        end
        
        % making sure the selected criterion is available
        assert(ismember(method,PermittedSkills),['Skill (' method ') not supported for cross-validation type (' pattern ').'])
        
        % ********************************************************************************
        % Actual cross-validation loop
        % ********************************************************************************
        
        % FOR EVERY BLOCK:
        for iBlock=1:nBlock
            
            %disp(['	Block ' num2str(iBlock) ' of ' num2str(nBlock)])
            
            % select the rows with validation data
            rowVal  =   find(blockindex==blocks(iBlock))    ;
            % select the rows with calibration data
            rowCal  =	setdiff(1:nSample,rowVal)           ;
            
            % =================================
            %   MATRIX DECOMPOSITION
            % =================================
            
            % compute mean and subtract it (if needed) and decompose with
            % SVD:
            switch model
                case 'kppca'
                    % currently only for 2D:
                    Ycal = Ytil(rowCal,:);
                    Yval = Ytil(rowVal,:);
                    K0cal = exp( -( (Ycal(:,1)-Ycal(:,1)').^2+(Ycal(:,2)-Ycal(:,2)').^2 )/(2*sigmak^2) );
                    K0val = exp( -( (Yval(:,1)-Ycal(:,1)').^2+(Yval(:,2)-Ycal(:,2)').^2 )/(2*sigmak^2) );
                    %K0cal = (Ycal*Ycal'+1).^sigmak;
                    %K0val = (Yval*Ycal'+1).^sigmak;
                    K1cal = K0cal; %-mean(K0cal)-mean(K0cal)'+mean(K0cal(:)) ;
                    K1val = K0val; %-mean(K0cal)-mean(K0val')'+mean(K0cal(:)) ;
                    [mu,sigma,lambda2o,V]= fDecompose(K1cal,centering,scaling) ;
                case 'ppca'
                    [mu,sigma,lambda2o,V]= fDecompose(Ytil(rowCal,:),centering,scaling) ;
                case 'blockppca'
                    [mu,sigma,lambda2o,V,U,S]= fDecompose(Ytil(rowCal,:),centering,scaling,'em',Kmax,originindex(rowCal));
                otherwise
            end
            switch pattern
                case 'rkf'
                    Kstart = Kmax;
                case 'ekf'
                    Kstart = Kmax;
            end
            for K=Kstart:-1:0
                
                % =================================
                %   PPCA MODEL SETUP
                % =================================
                
                % Compute additional PPCA model elements,
                %   i.e. noise variance (sigma2), eigenvalues of the
                %   denoised data (phi2) and eigenvalues of the noisy data
                %   (lambda2)
                [sigma2,phi2,lambda2]=fPPCAconstruct(lambda2o,K,J) ;
                
                % =================================
                %   PPCA VALIDATION
                % =================================
                switch pattern
                    case 'rkf'
                        switch method
                            case {'ign','ignres'}
                                % compute the ignorance score and the
                                % ignorance score in the residual space for
                                % the validation sample
                                switch model
                                    case 'kppca'
                                        skill(rowVal,1,K+1,:) = fPPCAvalidate(K1val,mu,sigma,lambda2,V,J,[],K) ;
                                    case 'ppca'
                                        skill(rowVal,1,K+1,:) = fPPCAvalidate(Ytil(rowVal,:),mu,sigma,lambda2,V,J,[],K) ;
                                    case 'blockppca'
                                        skill(rowVal,1,K+1,:) = fPPCAvalidate(Ytil(rowVal,:),mu,sigma,lambda2,V,J,[],K,1) ;
                                    otherwise
                                end
                            otherwise
                                disp(['         Skill (' method ') not supported for cross-validation type (' pattern ').'])
                        end
                    case 'ekf'
                        for jMissing=1:J
                            % compute the ignorance score, the squared
                            % error, and the continuous ranked probability
                            % score (crps) for the validation sample
                            [ign_val,error2,cprs_val]  = fPPCAvalidate(Ytil(rowVal,:),mu,sigma,lambda2,V,J,jMissing) ;
                            skill(rowVal,jMissing,K+1,:) = [ign_val,error2,cprs_val] ;
                        end
                        
                end
            end
        end
        
        % select the subset of the computed matrix "skill" to obtain the
        % criterion that is wanted.
        switch method
            case 'ign'
                skill = (skill(:,:,:,1)) ;
            case {'ignres','msr'}
                skill = (skill(:,:,:,2)) ;
            case {'crps'}
                skill = (skill(:,:,:,3)) ;
            otherwise
                disp(['         Skill (' method ') not supported for cross-validation type (' pattern ').'])
        end
    case {'efa','efa1','efa2'} % exploratory factor analysis
        disp(['      Cross-validation ' upper(model) ' model'])
        % ********************************************************************************
        % Prellocation
        % ********************************************************************************
        switch pattern
            case 'rkf'
                PermittedSkills =	{'ign','ignres'}            ;
                skill           =	zeros(nSample,1,Kmax+1,2)	;
            case 'ekf'
                PermittedSkills =	{'crps','ign','msr'}        ;
                skill           =	zeros(nSample,J,Kmax+1,3) ;
            otherwise
                disp(['         CV method (' pattern ') not supported for model type (' model ').'])
        end
        
        % making sure the selected criterion is available
        assert(ismember(method,PermittedSkills),['Skill (' method ') not supported for cross-validation type (' pattern ').'])
        
        % ********************************************************************************
        % Actual cross-validation loop
        % ********************************************************************************
        
        % FOR EVERY BLOCK:
        for iBlock=1:nBlock
            
            %disp(['	Block ' num2str(iBlock) ' of ' num2str(nBlock)])
            
            % select the rows with validation data
            rowVal  =   find(blockindex==blocks(iBlock))    ;
            % select the rows with calibration data
            rowCal  =	setdiff(1:nSample,rowVal)           ;
            
            if centering
                mu  = mean(Ytil,1)   ;
            else
                mu  = zeros(1,J)  ;
            end
            Yc     = Ytil-mu        ;
            if scaling
                sigma = std(Yc,[],1);
            else
                sigma = ones(1,J) ;
            end
            Ycs = Yc./sigma ;
            
            for K=Kmax:-1:0
                
                %disp([' Factors:' num2str(K) ])
                
                % =================================
                %   MATRIX DECOMPOSITION
                % =================================
                
                % compute mean and subtract it (if needed) and decompose with
                % Factor Analysis, report result in PCA format:
                [mu1,sigma1,lambda2o,V]= fDecompose(Ycs(rowCal,:),0,0,model,K) ;
                
                % =================================
                %   EFA MODEL SETUP
                % =================================
                
                %svd(V'*V+diag(la)
                % Compute additional PPCA model elements,
                %   i.e. noise variance (sigma2), eigenvalues of the
                %   denoised data (phi2) and eigenvalues of the noisy data
                %   (lambda2)
                [sigma2,phi2,lambda2]=fPPCAconstruct(lambda2o,K) ;
                
                % =================================
                %   PPCA VALIDATION
                % =================================
                switch pattern
                    case 'rkf'
                        switch method
                            case {'ign','ignres'}
                                % compute the ignorance score and the
                                % ignorance score in the residual space for
                                % the validation sample
                                skill(rowVal,1,K+1,:) = fPPCAvalidate(Ytil(rowVal,:),mu,sigma,lambda2,V,J,[],K) ;
                            otherwise
                                disp(['         Skill (' method ') not supported for cross-validation type (' pattern ').'])
                        end
                        
                    case 'ekf'
                        for jMissing=1:J
                            % compute the ignorance score, the squared
                            % error, and the continuous ranked probability
                            % score (crps) for the validation sample
                            [ign_val,error2,cprs_val]  = fPPCAvalidate(Ytil(rowVal,:),mu,sigma,lambda2,V,J,jMissing) ;
                            skill(rowVal,jMissing,K+1,:) = [ign_val,error2,cprs_val] ;
                        end
                        
                end
            end
        end
        
        % select the subset of the computed matrix "skill" to obtain the
        % criterion that is wanted.
        switch method
            case 'ign'
                skill = (skill(:,:,:,1)) ;
            case {'ignres','msr'}
                skill = (skill(:,:,:,2)) ;
            case {'crps'}
                skill = (skill(:,:,:,3)) ;
            otherwise
                disp(['         Skill (' method ') not supported for cross-validation type (' pattern ').'])
        end
        
    otherwise
        disp('         unknown model type')
        
end

% ********************************************************************************
% Averaging and jack-knifing
% ********************************************************************************

dims                      =   size(skill)                                       ;
Z                         =   reshape(skill,[ prod(dims(1:2)) dims(3:end) ])	;
[skill_mu,skill_mu_sigma] =   fJackKnife(Z)                                     ;
skill_sigma               =   std(Z,[],1)                                       ;
