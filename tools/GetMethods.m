
% -------------------------------------------------------------------------
% PCA dimensionality selection - GetMethods.m
%
% Produce a cell array 'Methods' describing the list of models and
% cross-validation methods to test.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 21.05.2019
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Stefania Russo, Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


% Model/method combinations, given as:
%               model type	/	centering / scaling /	CV-pattern /	criterion
Methods	=   {   'pca' ,     	'mean' ,	'none' ,         'ekf' ,         'itr'	;
                'pca' ,     	'mean' ,	'none' ,         'ekf' ,         'pmp'	;
                'pca' ,     	'mean' ,	'none' ,         'ekf' ,         'tri'	;
                'pca' ,     	'mean' ,	'none' ,         'ekf' ,         'tsr'	;
                'pca' ,     	'mean' ,	'none' ,         'ekf' ,         'citr'	;
                'pca' ,     	'mean' ,	'none' ,         'ekf' ,         'cpmp'	;
                'pca' ,     	'mean' ,	'none' ,         'ekf' ,         'ctri'	;
                'pca' ,     	'mean' ,	'none' ,         'ekf' ,         'ctsr'	;
                'ppca' ,     	'mean' ,    'none' ,         'ekf' ,         'ign'	;
                'ppca' ,     	'mean' ,    'none' ,         'rkf' ,         'ign'	;
                'pca' ,     	'mean' ,    'none' ,         '' ,         'vre'	}   ;
%  Methods	=   {   'ppca' ,     	'mean' ,    'none' ,         'ekf' ,         'ign'	;
%                  'ppca' ,     	'mean' ,    'none' ,         'rkf' ,         'ign'	;
%                  'pca' ,     	'mean' ,    'none' ,         '' ,         'vre'	}   ;
%Methods	=   {   'ppca' ,     	'mean' ,    'none' ,         'rkf' ,         'ign'	}   ;
