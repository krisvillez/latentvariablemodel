function [ign_val,error2,crps_val,Yhat]  = fPPCAvalidate(Yval,mu_hat,sigma_hat,lambda2_hat,V,J,jMissing,K,block)

% -------------------------------------------------------------------------
% PCA dimensionality selection - fPPCAvalidate.m
%
% Validate a PPCA model on new data
%
% Syntax: 	[ign_val,error2,crps_val,Yhat]  = fPPCAvalidate(Yval,mu_hat,sigma_hat,lambda2_hat,V,N,colM,K,block)
%
%	Inputs: 	
%       Yval        Data to validate the model
%       mu_hat      Center
%       sigma_hat   Scaling factors
%       lambda2_hat	Estimated variances
%       V           Eigenvectors
%       J           Number of variables           
%       jMissing    [optional] Index of column with missing data; empty
%                   value means ignorance score is computed for whole
%                   samples (row-wise validation), otherwise element-wise
%                   validation is used
%       K           [optional] Number of principal components, only used
%                   to compute whole-sample ignorance score in residual space
%       blockindex  [optional] Column vector with integer index indicating
%                   the block of replicates the sample belongs too, useful
%                   to constrain the latent variable scores to be the same for replicates
%
%	Outputs: 	
%		ign_val     Ignorance scores (column 1: complete PPCA model, column
%                   2: residual space only)
%       error2      Squared imputation error (mean vs measurement)
%                   (!) only available with element-wise validation
%       crps_val    continuous ranked probability score
%                   (!) only available with element-wise validation
%       Yhat        IxJ data matrix with I samples, J variables and
%                   variable in column jMissing replaced with the imputed
%                   value
%                   (!) only available with element-wise validation
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 21.05.2019
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Stefania Russo, Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<9 || isempty(block)
    block=false;
end
% function used to validate PPCA model

if (nargin >=6 && ~isempty(jMissing)) && length(jMissing)==1
    % IF: validating based on imputation
    
    Yvalms	=   (Yval-mu_hat)./sigma_hat                ;
    
    % variables that are not missing
    jAvail	=	[1:(jMissing-1) (jMissing+1):J];
    
    % compute submatrices of covariance matrix
    C22     =	V(jAvail,:)*diag(lambda2_hat)*V(jAvail,:)'	;
    C11     =	V(jMissing,:)*diag(lambda2_hat)*V(jMissing,:)'	;
    C21     =	V(jAvail,:)*diag(lambda2_hat)*V(jMissing,:)'	;
    C12     =	C21'                                    ;
    
    P = (C22)\C21	;
    
    % compute mean estimate
    Ymiss_hat       =	Yvalms(:,jAvail)*P;
    % variance of estimate
    sigma2_miss_hat	=	C11-C12*P          ;
    % standard deviation of estimate
    sigma_miss_hat	=	sigma2_miss_hat.^(1/2)          ;
    
    % compute Z-score of residual
    Z               =	(Ymiss_hat-Yvalms(:,jMissing))/sigma_miss_hat	;
    if block
        Z           =   repmat(mean(Z,1),[size(Z,1) 1])   ;
    end
    
    % compute log-likelihood
    ign_val = log(2*pi)*1/2+ sum( log(sigma_miss_hat))+ (   ( Z .^ 2 / 2) )   ;
    
    % compute squared residual (in original scale)
    if nargout>=2
        error2 = ( (Ymiss_hat.*sigma_hat(jMissing)+mu_hat(jMissing) )-Yval(:,jMissing) ).^2 ;
    end
    
    % compute continuous ranked probability score (CRPS)
    if nargout >=3
        phi         =	(2 * pi)^(- 1/2) * exp (- Z .^ 2 / 2)               ;
        PHI         =	erfc (Z / (-sqrt(2))) / 2                           ; 
        crps_val	=	-sigma_miss_hat*(1/(sqrt(2*pi))-2*phi-Z.*(2*PHI-1)) ;
    end
    
    Yhat = Yvalms;
    Yhat(:,jMissing) = Ymiss_hat;
    Yhat = Yhat.*sigma_hat+mu_hat ;
    
else
    
    % ELSE: validating whole sample at once
    Yhat = Yval;
    
    Yms = ((Yval-mu_hat)./sigma_hat) ;
    if block
        R = size(Yms,1) ;
        Yms = Yms'; % RxN
        Yms = Yms(:); % R.N x 1
        Vb = repmat(V,[R 1]); % R.NxC
        T = pinv(Vb)*Yms ;
        T = repmat(T(:)',[ R 1 ]) ;
    else
        T       =   Yms*V ;
    end
    
    dMahalanobis =  sum( T.^2./lambda2_hat(:)',2);
    ign_val =   +1/2*(log(2*pi)*J+ sum( log(lambda2_hat))+dMahalanobis)  ;
    
    error2 = dMahalanobis;
    if (nargin >=7 && ~isempty(K)) && length(K)==1
        dMahalanobis =  sum( T(:,K+1:end).^2./lambda2_hat(K+1:end)',2);
        ign_val(:,2) =    +1/2*(log(2*pi)*(J-K)+ sum( log(lambda2_hat(K+1:end)))+dMahalanobis)  ;
    end
    
end