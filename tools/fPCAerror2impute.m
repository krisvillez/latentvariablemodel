function [error2,Yhat] = fPCAerror2impute(Y,mu,sigma,lambda2,V,J,jMissing,method,B) 

% -------------------------------------------------------------------------
% PCA dimensionality selection - fPCAerror2impute.m
%
% Produce an imputed value and the squared imputation error for a single
% missing variable 
%
% Syntax: 	[error2,Yhat] = fPCAerror2impute(Y,mu,sigma,lambda2,V,J,jMissing,skillmethod,B) 
%
%	Inputs: 	
%       Y           IxJ data matrix with I samples, J variables
%		mu          Applied center
%       sigma       Applied scaling factors
%       lambda2     Eigenvalues
%       J           Number of variables
%       jMissing    Index of missing variable
%       method      Imputation method ('itr','pmp','tri','tsr','citr','cpmp','ctri','ctsr')
%       B           Regression coefficients (for 'tsr' and 'ctsr' method only)
%
%	Outputs: 	
%       error2      squared imputation error
%       Yhat        IxJ data matrix with I samples, J variables and
%                   variable in column jMissing replaced with the imputed
%                   value
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 21.05.2019
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Stefania Russo, Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% centering:
Yc           =   (Y-mu)./sigma                ; 

switch method
    case {'itr','citr'} % iterative estimation
        
        % mean centering
        
        % initialization
        iternum     =   0                   ;
        M           =   size(Y,1)           ;
        relchange	=	inf(M,1)            ;
        Yest        =	zeros(M,1)          ;
        error2      =	(Yest-Yc(:,jMissing)).^2 ;
        
        % as long as there is no convergence yet
        while any(relchange>1e-3)
            
            % iteration update
            iternum         =   iternum+1                           ;
            
            % apply model with current estimate
            Ytag            =   Yc                                   ;
            Ytag(:,jMissing)=   Yest                                ;
            Ttag            =	Ytag*V                              ;
            
            % reconstruct estimate
            Yest            =	Ttag*V(jMissing,:)'                     ;
            
            % compute new squared error and relative change
            error2_new      =	(Yest-Yc(:,jMissing)).^2                 ;
            relchange       =	abs(error2_new-error2)./abs(error2) ;
            error2          =	error2_new                          ;
            
        end
        
        % return complete input matrix with estimate
        Ytag(:,jMissing)    =   Yest	;
        Yhat            =	Ytag	;
        
    case {'pmp','cpmp'} % projection to model plane - numerically unstable. Use iterative estimation as equivalent method.
        
        % variables that are not missing
        jAvail	=	[1:(jMissing-1) (jMissing+1):J];
        
        % initialize estimates
        Yhat	=	Yc                  ;
        
        % estimate missing variable
        if isempty(lambda2)
            Yhat(:,jMissing)	=	0       ;
        else
            Yhat(:,jMissing)	=	Yhat(:,jAvail)*V(jAvail,:)*((V(jAvail,:)'*V(jAvail,:))\V(jMissing,:)')	;
        end
        
    case {'tri','ctri'} % trimmed score imputation
        
        % initialize estimates
        Ytag            =   Yc      ;
        Ytag(:,jMissing)    =   0       ;
        
        % projection
        Ttag            =	Ytag*V          ;
        
        % reconstruction
        Yest            =	Ttag*V(jMissing,:)' ;
        
        % return complete input matrix with estimate
        Ytag(:,jMissing)    =   Yest    ;
        Yhat            =	Ytag	;
        
    case {'tsr','ctsr'} % trimmed score regression
        
        % initialize estimates
        Ytag            =   Yc      ;
        Ytag(:,jMissing)    =   0       ;
        
        % projection
        Ttag            =	Ytag*V	;
        
        % regression
        That            =	Ttag*B	;
        
        % reconstruction
        Yest            =	That*V(jMissing,:)'	;
        
        % return complete input matrix with estimate
        Ytag(:,jMissing)    =   Yest    ;
        Yhat            =	Ytag	;
        
    otherwise
        disp(['Imputation method (' skillmethod ') not supported.'])
end

% add mean vector
Yhat	=	Yhat.*sigma+mu              ;

% compute squared residual
error2	=	(Yhat(:,jMissing)-Y(:,jMissing)).^2	;