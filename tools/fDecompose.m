function [mu,sigma,lambda2,V,U,S]= fDecompose(Y,center,scaling,method,nFactor,blockindex)

% -------------------------------------------------------------------------
% PCA dimensionality selection - fDecompose.m
%
% Decompose a data matrix according to a latent variable model, either
% principal component analysis (PCA) or exploratory factor analysis (EFA).
% Note that the outputs associated with the EFA model are given in PCA
% form, i.e. EFA is used to estimate the covariance matrix, which is then
% used to compute the PCA model associated with the estimated covariance
% matrix and the corresponding principal scores.
%
% Syntax: 	[mu,sigma,lambda2,V,U,S]= fDecompose(Y,center,scaling,method,nFactor,blockindex)
%
%	Inputs: 	
%       Y           IxJ data matrix with I samples, J variables
%       center      Centering method ('mean' or 'no')
%       scaling     Scaling method ('unitvar' or 'no')
%       method      Decomposition method
%                   (PCA models: 'svd' or 'em', EFA models: 'efa','efa1', or 'efa2' ) 
%       nFactor     Number of latent variables (optional with 'svd', not used in this case)
%       blockindex  [optional] COlumn vector with integer index indicating
%                   the block of replicates the sample belongs too, useful
%                   to constrain the latent variable scores to be the same for replicates
%
%	Outputs: 	
%		mu          Applied center
%       sigma       Applied scaling factors
%       lambda2     Eigenvalues
%       V           Eigenvectors
%       U           Principal scores
%       S           Diagonal matrix with singular values
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 21.05.2019
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Stefania Russo, Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


if nargin<4 || isempty(method)
    method ='svd';
end

% number of sample, number of observed variables
[M,N] = size(Y) ;

% blockindex
if nargin<6 || isempty(blockindex)
    blocks	=	[];
else
    blockindex  =   blockindex(:)       ;
    [blockindex,index]=sort(blockindex);
    Y =Y(index,:) ;
    assert(length(blockindex)==M,'Dimensions of "blockindex" do not match row size of "Ytil".')
    blocks          =   unique(blockindex)  ;
    
end
nBlock          =   length(blocks)      ;

% ==============================
% CENTERING
% ==============================
switch center
    case 'mean'
        mu  = mean(Y,1)   ;
    otherwise
        mu  = zeros(1,N)  ;
end
Y    = Y-mu        ;
switch scaling
    case 'unitvar'
        sigma = std(Y,[],1);
    otherwise
        sigma = ones(1,N) ;
end
Y = Y./sigma ;
% ==============================
% ==============================
method = lower(method);

switch method
    case 'svd'
        % ==============================
        % SVD DECOMPOSITION
        % ==============================
        [U,S,V] = svd(Y,0)            ;
        lambda2 = diag(S).^2/M        ; % assumes input matrix is data, not a covariance matrix (!)
        lambda2 = lambda2(:)          ;
        
        % ==============================
        % ==============================
    case 'em'
        
        % Approach assumes input matrix is data, not a covariance matrix (!)
        
        % Initialize model
        [U,S,V] = svd(Y,0)            ;
        T   =   U*S     ;
        T   =   T(:,1:nFactor)       ;
        P   =   V(:,1:nFactor)       ;
        
        if nBlock==0
        else
            J = inf;
            iter= 0;
            itermax =1000;
            mu1=zeros(N,1);
            if nFactor==0
                if center
                    T1 = [ ones(M,1) ];
                    mP = Y'*T1/(T1'*T1);
                    mu1 = mP(:,1);
                else
                    mu1= zeros(N,1);
                end
                Yhat    =  repmat(mu1(:)',[M 1]); % estimated data in scaled form
            else
                while iter<itermax
                    iter=iter+1;
                    % update scores
                    for iBlock=1:nBlock
                        rows = find(blockindex==blocks(iBlock));
                        Yblock = Y(rows,:)';
                        if center
                            Yblock =Yblock-mu1 ;
                        end
                        Yblock = Yblock(:)' ;
                        R = length(rows) ;
                        Pblock = repmat(P,[ R 1 ]) ;
                        % obtain new scores via regression:
                        %   minimize_{T} sum( (Yblock - Tblock*Pblock').^2 )
                        %   N*R         N*R x C     x	CxC
                        Tblock = Yblock	*   Pblock      / (Pblock'*Pblock) ;
                        %Tblock = Yblock	*   Pblock      *   eye(nFactor)*1/R ;
                        T(rows,:)  = repmat(Tblock,[ R 1]) ;
                    end
                    
                    % update components
                    %   minimize_{P} sum( (Yblock - (1 * mu'+T*P')).^2 )
                    %   N*R         N*R x C     x	CxC
                    %(Y'*T*inv(T'*T)) = P
                    if center
                        T1 = [ ones(M,1) T ];
                        mP = Y'*T1/(T1'*T1);
                        mu1 = mP(:,1);
                        P = mP(:,2:end);
                    else
                        P = Y'*T/(T'*T);
                        mu1= zeros(N,1);
                    end
                    v = sum(P.^2,1);
                    P=P./(v.^(1/2));
                    
                    % evaluate model
                    Jnew = norm(Y-T*P');
                    if ((J-Jnew)/Jnew)<(1e-9)
                        iter = inf;
                    end
                    J=Jnew;
                end
                Yhat    =   (T*P'+mu1(:)'); % estimated data in scaled form
            end
            
        end
        
        if center
            % new mean
            mu1 = mean(Yhat);
            % modify known mean in original scale:
            mu = mu+mu1.*sigma ;
            % re-center modelled data
            Yhat=Yhat-mu1(:)' ;
        end
        [U,S,V] = svd(Yhat,0)            ;
        lambda2 = diag(S).^2/M        ; % assumes input matrix is data, not a covariance matrix (!)
        lambda2 = lambda2(:)          ;
        
    case {'efa','efa1','efa2'}
        
        stdev = std(Y,1);
        Ys = Y./stdev(:)';
        
        switch method
            case 'efa'
                optimset = statset('factoran')   ;
                optimset.MaxFunEvals = inf;
                optimset.MaxIter = inf;
                optimset.TolFun = 1e-11 ;
                optimset.TolX = 1e-11 ;
                corrY= (Ys'*Ys)/M;
                [V,lambda2]=factoran(corrY,nFactor,'Xtype','covariance','Rotate','none','start','Rsquared','optimopts',optimset,'Delta',1e-5);
                
            case 'efa1'
                [B,L,var,fac,E] = FA(Ys,nFactor,inf,1e-6);
                V = B(:,1:end-1);
                lambda2 = B(:,end);
                
            case 'efa2'
                [V,lambda2]=ffa(Ys,nFactor);
        end
        
        % Construct estimated covariance matrix according to obtained model:
        CovY = diag(stdev)*(V*V'+diag(lambda2))*diag(stdev);
        % Decompose estimated covariance matrix to obtain in standard PCA
        % form:
        [U,S,V] = svd(CovY,0)            ;
        lambda2= diag(S);
        if nargout>=4
            U=Y*V*diag(1./lambda2.^(1/2));
        end
end