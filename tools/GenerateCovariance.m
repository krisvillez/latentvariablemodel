more off
clc
clear all
close all

SetName = 'B1' ;

addpath('tools')

filepath    =   ['tex' SetName] ;
fileID      =	fopen (filepath,'w')                            ;

SetName = 'B2';
for type=1:18
    
    [Sigma2,nOVsim,Ktrue]	=	fGenerateCovariance(SetName,type,1) ; % currently complete: A,B1,B2,C
    
    s = [ '\paragraph{Data type ' SetName '.' num2str(type) '}' ];
    fprintf (fileID, '%s\n', s )                         ;
    s = [ ' ' ];
    fprintf (fileID, '%s\n', s )                         ;
    s = [ '\begingroup \everymath{\tiny} \begin{align}' ];
    fprintf (fileID, '%s\n', s )                         ;
    s = [ '\Sigma & = \frac{1}{10} \left[ \begin{array}{' repmat('c',[1 nOVsim]) '}' ];
    fprintf (fileID, '%s\n', s )                         ;
    for iOV=1:nOVsim
        fprintf (fileID, [ repmat('%2d	&	',1,nOVsim-1) '%2d  \\\\ \n'], Sigma2(iOV,:)*10 )                         ;
    end
    s = [ '\end{array} \right]' ];
    fprintf (fileID, '%s\n', s )                         ;
    s = [ '\end{align} \endgroup' ];
    fprintf (fileID, '%s\n', s )                         ;
    s = [ ' ' ];
    fprintf (fileID, '%s\n', s )                         ;
    
end

fclose(fileID);