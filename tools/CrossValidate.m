
% -------------------------------------------------------------------------
% PCA dimensionality selection - CrossValidate.m
%
% Script which tests all combinations of model types and cross-validation
% methods.
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 21.05.2019
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Stefania Russo, Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


% Cross-validation:
for iMethod = 1:nMethod
    
    disp(['     Model/method combo: ' num2str(iMethod) ' of ' num2str(nMethod) ])
    
    % -----------------------------------------------------------------
    %   Get model specifications and cross-validation criteria
    % -----------------------------------------------------------------
    method              =   Methods(iMethod,:)	;
    model               =   method{1}           ;
    centering           =   method{2}           ;
    scaling             =   method{3}           ;
    method_cv           =   method{4}           ;
    method_estimation   =   method{5}           ;
    
    % -----------------------------------------------------------------
    %   Execute cross-validation
    % -----------------------------------------------------------------
    
    time0               =   tic                 ;
    if strcmp(method_estimation,'vre')
        criterion       =	fVRE(Ytil,model,centering,scaling) ;
    else
        criterion       =	fCrossvalidation(Ytil,model,centering,scaling,method_cv,method_estimation,blocks)  ;
    end
    timeused            =	toc(time0)          ;
    
    % -----------------------------------------------------------------
    %   Post-process results
    % -----------------------------------------------------------------
    
    if strcmp(method_cv,'rkf')
        criterion = criterion/J ;
    end
    nCrit = length(criterion) ;
%     
%     figure, plot(criterion)
%     pause
    % -----------------------------------------------------------------
    %   Store results
    % -----------------------------------------------------------------
    
    MethodName      = [ 'Model' model '-' centering '-' scaling '_CV' method_cv '-' method_estimation ];
    filepath        = [ path_out filenamebase '_' MethodName  '.csv' ] ;
    
    if Simulation==true
        if iRep==0
            % open file
            fileID          =	fopen (filepath,'w')                            ;
            % write header
            formatLine      =   [ repmat('%s;',1,nCrit+2) '\n'  ];
            heads = cellstr([repmat('PC',nCrit,1) num2str((0:(nCrit-1))')])' ;
            heads = [ {'Simulation'} , {'Time'} , heads ];
            fprintf (fileID, formatLine, heads{:} )                         ;
            rightone = cellstr([num2str((0:(nCrit-1))'==Ktrue)])' ;
            rightone = [ {'Nan'} , {'Nan'} , rightone ];
            fprintf (fileID, formatLine, rightone{:} )                         ;
        else
            % open file
            fileID          =	fopen (filepath,'a')    ;
        end
        
        % write data
        formatLine      = [  '%3d;%12.6f;' repmat('%12.6f;',1,nCrit) '\n' ];
        out = num2cell([ iRep ; timeused ; criterion(:) ]);
        fprintf (fileID, formatLine, out{:} )                         ;
        
    else
        if mod==0
            % open file
            fileID          =	fopen (filepath,'w')                            ;
            % write header
            formatLine      =   [ repmat('%s;',1,nCrit+2) '\n'  ];
            heads = cellstr([repmat('PC',nCrit,1) num2str((0:(nCrit-1))')])' ;
            heads = [ {'Simulation'} , {'Time'} , heads ];
            fprintf (fileID, formatLine, heads{:} )                         ;
            rightone = cellstr([num2str((0:(nCrit-1))'==Ktrue)])' ;
            rightone = [ {'Nan'} , {'Nan'} , rightone ];
            fprintf (fileID, formatLine, rightone{:} )                         ;
        else
            % open file
            fileID          =	fopen (filepath,'a')    ;
        end
        
        % write data
        formatLine      = [  '%3d;%12.6f;' repmat('%12.6f;',1,nCrit) '\n' ];
        out = num2cell([ mod ; timeused ; criterion(:) ]);
        fprintf (fileID, formatLine, out{:} )                         ;
        
    end
    
    % close file
    fclose(fileID);
    drawnow()
    
end