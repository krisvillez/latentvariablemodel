function [V_hat,lambda2_hat]=fPCAconstruct(lambda2,K,V)

% -------------------------------------------------------------------------
% PCA dimensionality selection - fPCAconstruct.m
%
% Produce the PCA model corresponding to a particular number of principal
% components. 
%
% Syntax: 	[V_hat,lambda2_hat]=fPCAconstruct(lambda2,K,V)
%
%	Inputs: 	
%       lambda2     Eigenvalues
%       K           Number of principal components
%       V           Eigenvectors
%
%	Outputs: 	
%       V_hat           Selected eigenvectors
%       lambda2_hat     Selected eigenvalues
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 21.05.2019
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Stefania Russo, Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% construct PCA model
LVmax       =   length(lambda2) ;	% maximal number of PCs
K           =   min([K LVmax])  ;
lambda2_hat	=   lambda2(1:K)    ;	% selected eigenvalues
V_hat       =	V(:,1:K)        ;   % selected eigenvectors