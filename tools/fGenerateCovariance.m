function [Sigma2,nOV,nLV,sigma2,type] = fGenerateCovariance(SetName,Type,NoiseLevel)

% Generate covariance matrix according to selected type of data and
% instance of it. Data set name and instance indexing is applied as in [1].
%
% References:
%   [1] Saccenti, E., & Camacho, J. (2015). Determining the number of
%       components in principal components analysis: A comparison of
%       statistical, crossvalidation and approximated methods. Chemometrics
%       and Intelligent Laboratory Systems, 149, 99-116.

switch SetName
    case {'A','D'}
        CaseNumber = Type ;
        nOV     =	50          ;	% number of observed variables
        lambda2	=	ones(nOV,1) ;   % initialize eigenvalues
        switch CaseNumber
            case num2cell(1:10)
                lambda      =	CaseNumber          ;   % 1st eigenvalue
                lambda2(1)	=	lambda              ;   % overwrite 1st eigenvalue
            case num2cell(11:15)
                lambda      =	(CaseNumber-10)*2	;   % 1st eigenvalue
                lambda2(1:2)=   [ lambda lambda-1 ] ;   % overwrite 1st-2nd eigenvalue
            case num2cell(16:20)
                lambda      =	(CaseNumber-15)*2+1         ;   % 1st eigenvalue
                lambda2(1:3)=	[ lambda lambda-1 lambda-2 ];	% overwrite 1st-3rd eigenvalue
            case num2cell(21:25)
                lambda      =	(CaseNumber-20)*2+2                     ;   % 1st eigenvalue
                lambda2(1:4)=	[ lambda lambda-1 lambda-2  lambda-3 ]	;	% overwrite 1st-4th eigenvalue
            otherwise
                disp('Unknown test case')
        end
        Sigma2	=	diag(lambda2)               ;   % Create covariance matrix as diagonal matrix
        nLV     =	sum(lambda2>lambda2(end))   ;   % Compute correct number of principal components
    case {'B1','B2'}
        CaseNumber = Type ;
        switch SetName
            case 'B1',  fac	=	1   ;
            case 'B2',  fac =	2   ;
            otherwise,  disp('Unknown test case')
        end
        switch CaseNumber
            case num2cell(1:8)
                b1	=	4   ;
                b2	=	7   ;
                b3	=	9   ;
            case num2cell(9:18)
                b1	=	3   ;
                b2	=	6   ;
                b3	=	9	;
            otherwise
                disp('Unknown test case')
        end
        block1	=	1:(b1*fac)          ;
        block2  =	(b1*fac+1):b2*fac	;
        block3  =	(b2*fac+1):b3*fac	;
        nOV     =	b3*fac              ;
        
        switch CaseNumber
            case 1
                val0 = 0.0 ; val1 = 0.8;   val2 = 0.8;   val3 = 0.8;
            case 2
                val0 = 0.3 ; val1 = 0.8;   val2 = 0.8;   val3 = 0.8;
            case 3
                val0 = 0.5 ; val1 = 0.8;   val2 = 0.8;   val3 = 0.8;
            case 4
                val0 = 0.0 ; val1 = 0.5;   val2 = 0.5;   val3 = 0.5;
            case 5
                val0 = 0.2 ; val1 = 0.5;   val2 = 0.5;   val3 = 0.5;
            case 6
                val0 = 0.0 ; val1 = 0.3;   val2 = 0.3;   val3 = 0.3;
            case 7
                val0 = 0.0 ; val1 = 0.8;   val2 = 0.5;   val3 = 0.3;
            case 8
                val0 = 0.1 ; val1 = 0.8;   val2 = 0.5;   val3 = 0.3;
            case 9
                val0 = 0.0 ; val1 = 0.8;   val2 = 0.5;   val3 = 0.3;
            case 10
                val0 = 0.0 ; val1 = 0.8;   val2 = 0.5;   val3 = 0.0;
            case 11
                val0 = 0.0 ; val1 = 0.5;   val2 = 0.3;   val3 = 0.0;
            case 12
                val0 = 0.0 ; val1 = 0.8;   val2 = 0.0;   val3 = 0.0;
            case 13
                val0 = 0.0 ; val1 = 0.5;   val2 = 0.0;   val3 = 0.0;
            case 14
                val0 = 0.0 ; val1 = 0.3;   val2 = 0.0;   val3 = 0.0;
            case 15
                val0 = 0.8 ; val1 = val0;   val2 = val0;   val3 = val0;
            case 16
                val0 = 0.5 ; val1 = val0;   val2 = val0;   val3 = val0;
            case 17
                val0 = 0.3 ; val1 = val0;   val2 = val0;   val3 = val0;
            case 18
                val0 = 0.0 ; val1 = val0;   val2 = val0;   val3 = val0;
            otherwise
                disp('Unknown test case')
        end
        
        % Construct covariance matrix
        Sigma2                  =	ones(nOV,nOV)*val0                  ;
        Sigma2(block1,block1)	=	val1                                ;
        Sigma2(block2,block2)	=	val2                                ;
        Sigma2(block3,block3)	=	val3                                ;
        Sigma2                  =	Sigma2-diag(diag(Sigma2))+eye(nOV)	;
        
        % Compute eigenvalues ( [!] CONSIDER USING SVD INSTEAD )
        lambda2                 =	eigs(Sigma2,nOV)                    ;
        
        % Compute true number of principal components
        nLV                     =	sum( (lambda2./(lambda2(end))>(1+1e-6)));
    case {'C','E'}
        
        sigma2values	=	[ 5:5:55 ]/100 ;
        n               =	length(sigma2values)                ;
                
        % select matrix type:
        switch Type
            
            case 1
                % initialize basis vectors
                nLV     =	4               ;
                nOV     =	10              ;
                P       =   zeros(nOV,nLV)	;
                
                % for every basis vector
                for i=1:10
                    switch i
                        case num2cell(1:5)
                            P(i,1:2)	=	[	(i/5).^(1/2)        ;
                                (1-i/5).^(1/2)	]   ;
                        case num2cell(6:9)
                            P(i,1:3)	=	[	sqrt(1/2)           ;
                                sqrt(i/10-.5)       ;
                                sqrt(1-i/10)	]	;
                        case 10
                            P(i,:)      =	[	sqrt(0.01)  ;
                                sqrt(0.01)  ;
                                sqrt(0.01)  ;
                                1           ]/sqrt(1.03) ;
                    end
                end
            case 2
                % initialize basis vectors
                nLV     =	8               ;
                nOV     =	10              ;
                P       =   zeros(nOV,nLV)  ;
                
                % construct every basis vector
                i                   =	0           ;
                for j=1:4
                    for k=(j+1):4
                        i           =	i+1         ;
                        P(i,[j k])	=	sqrt(1/2)   ;
                    end
                end
                
                for j=5:7
                    for k=(j+1):7
                        i           =   i+1         ;
                        P(i,[j k])	=	sqrt(1/2)   ;
                    end
                end
                
                i                   =   i+1         ;
                P(i,8)              =   1           ;
                
            case 3
                % initialize basis vectors
                nLV             =	12              ;
                nOV             =	27              ;
                P               =   zeros(nOV,nLV)  ;
                P(1:12,1:12)	=	eye(12)         ;
                
                % construct every basis vector
                i                   =   12          ;
                for j=1:6
                    for k=(j+1):6
                        i           =   i+1         ;
                        P(i,[j k])	=	sqrt(1/2)   ;
                    end
                end
                
            case 4
                % initialize basis vectors
                nLV     =	15              ;
                nOV     =	50              ;
                P       =   zeros(nOV,nLV)  ;
                
                % construct every basis vector
                i                   =   0           ;
                for j=1:10
                    for k=(j+1):10
                        i           =   i+1         ;
                        P(i,[j k])	=	sqrt(1/2)	;
                    end
                end
                
                i	=   i+1 ;   P(i,11)         =	1           ;
                i	=   i+1 ;   P(i,12)         =	1           ;
                i	=   i+1 ;   P(i,[11 13])	=	sqrt(1/2)	;
                i	=   i+1 ;   P(i,[12 14])	=	sqrt(1/2)	;
                i	=   i+1 ;   P(i,15)         =	1           ;
            otherwise
                disp('Unknown test case')
        end
        
        CovX        =	P*P'                                ;
        sigma2      =	sigma2values(NoiseLevel)            ;
        Sigma2      =	(CovX+eye(nOV)*sigma2)/(1+sigma2)   ;
        
    otherwise
        disp('Unknown test case')
end

end

