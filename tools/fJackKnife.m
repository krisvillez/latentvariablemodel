function [mu,sigma]=fJackKnife(Z)

% -------------------------------------------------------------------------
% PCA dimensionality selection - fJackKnife.m
%
% Produce the jack-knife mean and standard deviation
%
% Syntax: 	[mu,sigma]=fJackKnife(Z)
%
%	Inputs: 	
%       Z       Scores (column direction = samples)
%
%	Outputs: 	
%       mu      Mean
%       sigma	Standard deviation
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 21.05.2019
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Stefania Russo, Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

% Jack-knifing
nSample =	size(Z,1)                   ; % number of samples
mu      =	mean(Z,1)                   ; % compute mean criterion
Z       =	(mu*nSample-Z)/(nSample-1)	; % compute deviations of criterion from mean
sigma	=	std(Z,1)                    ; % standard deviation of deviations
