function VRE = fVRE(Ytil,model,centering,scaling)

% -------------------------------------------------------------------------
% PCA dimensionality selection - fVRE.m
%
% Produce the variance of reconstruction error
%
% Syntax: 	[mu,sigma]=fJackKnife(Z)
%
%	Inputs: 	
%       Ytil        IxJ data matrix with I samples, J variables
%       model       Model type (currently available: 'pca')
%       center      Centering method ('mean' or 'no')
%       scaling     Scaling method ('unitvar' or 'no')
%
%	Outputs: 	
%       VRE         variance of reconstruction error
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 21.05.2019
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Stefania Russo, Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


% number of samples, number of observed variables
[I,J]	=	size(Ytil)          ;

% compute maximum number of PCs, assuming Leave-One-Sample-Out scheme
switch model
    case 'kppca'
        Amax = I/2;
    otherwise
        Amax          = min(I-1,J-1)  ; % apply -1 since calibration data set has one sample less in LOOCV scheme.
        if strcmp(model(1:3),'efa')
            df2 = @(a,n) ((a-J)^2 - (a+J));
            while df2(Amax,I-1)<0
                Amax = Amax-1;
            end
        end
end

% fault directions:
XI      =	eye(J)      ;
nFault	=	size(XI,2)  ;

VRE = nan(Amax+1,1);
% select model structure:
switch model
    case 'pca'
        
        % compute mean and subtract it (if needed) and decompose with SVD:
        [mu,sigma,lambda2o,Vo]= fDecompose(Ytil,centering,scaling) ;
        
        Ysc = (Ytil-mu)./sigma ;
        SIGMAtil = ( Ysc'*Ysc )/I ;
        
        % =================================
        %   PCA MODEL SETUP
        % =================================
        
        for A=0:Amax
            

            % Compute/select PCA model elements,
            %   i.e. covariance matrix eigenvectors and eigenvalues
            [V,lambda2]=fPCAconstruct(lambda2o,A,Vo) ;
            
            XItil = (eye(J)-V*V')*XI ;
            
            vre = 0 ;
            for iFault=1:nFault
                f0 = ( XItil(:,iFault)'*XItil(:,iFault) ) ;
                f1 = ( XItil(:,iFault)'*SIGMAtil*XItil(:,iFault) ) ;
                f2 = ( XI(:,iFault)'*SIGMAtil*XI(:,iFault) ) ;
                term = f1/(f0.^2*f2) ;
                vre =vre + term ;
            end
            VRE(A+1) = vre/nFault ;
        end
        
        
    otherwise
end