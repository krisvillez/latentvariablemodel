function [sigma2_hat,phi2_hat,lambda2_hat]=fPPCAconstruct(lambda2,K,J)

% -------------------------------------------------------------------------
% PCA dimensionality selection - fPPCAconstruct.m
%
% Produce the variances for the PPCA model corresponding to a particular
% number of principal components.
%
% Syntax: 	[sigma2_hat,phi2_hat,lambda2_hat]=fPPCAconstruct(lambda2,K,J)
%
%	Inputs: 	
%       lambda2     Eigenvalues
%       K           Number of principal components
%       J           Number of variables
%
%	Outputs: 	
%       sigma2_hat	Estimated noise variance
%       phi2_hat    Estimated non-noisy variances
%       lambda2_hat	Estimated total variances associated with the
%                   directions described by the eigenvectors 
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 21.05.2019
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2018-2019 Stefania Russo, Kris Villez
%
% This file is part of the SOAR Toolbox for Matlab/Octave. 
% 
% The SOAR Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The SOAR Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SOAR Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


% construct PCA model

LVmax = length(lambda2) ;	% maximal number of PCs

% compute noise variance parameter:
if LVmax==K,	sigma2_hat	=	0                               ;
else,           sigma2_hat	=	sum(lambda2(K+1:end))/(LVmax-K) ;
end

% compute eigenvalues for denoised data:
phi2_hat            =   lambda2-sigma2_hat      ;
phi2_hat((K+1):J)	=   0                       ;

% compute eigenvalues for noisy data:
lambda2_hat         =   (phi2_hat+sigma2_hat)	;